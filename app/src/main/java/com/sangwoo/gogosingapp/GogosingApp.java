package com.sangwoo.gogosingapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kakao.auth.KakaoSDK;
import com.sangwoo.gogosingapp.Util.Encrypt.AES256Cipher;
import com.sangwoo.gogosingapp.Util.FBUtil.FBUtil;
import com.sangwoo.gogosingapp.Util.KakaoUtil.KaKaoSDKApapter;
import com.sangwoo.gogosingapp.Util.KakaoUtil.KakaoUtil;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerUtil;
import com.sangwoo.gogosingapp.Util.Util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by SangWoo on 2016. 7. 22..
 */
public class GogosingApp extends Application {

    private static Context mContext = null;
    private static Activity topActivity = null;
    private FBUtil fbUtil = null;
    private KakaoUtil kakaoUtil = null;
    private ServerUtil serverUtil = null;
    private boolean isLogin = false;
    private static String key32 = null;
    private static String key16 = null;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        initFacebook();
        initKakao();
        serverUtil = new ServerUtil();
        fbUtil = new FBUtil();
        kakaoUtil = new KakaoUtil();
        initKey();

        if (Util.getStringShared(Util.SHARED_INSTAILL_TIME) == null) {
            Util.setStringShared(Util.SHARED_INSTAILL_TIME, String.valueOf(System.currentTimeMillis()));
        }
    }

    public static GogosingApp getApp() {
        return (GogosingApp) mContext;
    }

    public static Context getContext() {
        return mContext;
    }


    public ServerUtil getServerUtil() {
        return serverUtil;
    }

    private void initFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    private void initKakao() {
        KakaoSDK.init(new KaKaoSDKApapter());
    }

    public static void setCurrentActivity(Activity activity) {
        topActivity = activity;
    }

    public static Activity getCurrentActivity() {
        return topActivity;
    }

    public FBUtil getFacebookUtil() {
        return fbUtil;
    }

    public KakaoUtil getKakaoUtil() {
        return kakaoUtil;
    }

    public static String byteArrayToHex(byte[] ba){
        if(ba == null || ba.length == 0){
            return null;
        }

        StringBuffer sb = new StringBuffer(ba.length * 2);
        String hexNumber;
        for(int x = 0; x < ba.length; x++){
            hexNumber = "0" + Integer.toHexString(0xff & ba[x]);

            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        return sb.toString();
    }

    private void initKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest mdSha = MessageDigest.getInstance("SHA-256");
                mdSha.update(signature.toByteArray());
                this.key32 = Base64.encodeToString(mdSha.digest(), Base64.URL_SAFE);
                Log.e("MY KEY HASH BASE64 32:", key32);

                MessageDigest mdSha128 = MessageDigest.getInstance("MD5");
                mdSha128.update(signature.toByteArray());
                this.key16 = Base64.encodeToString(mdSha128.digest(), Base64.URL_SAFE);
                Log.e("MY KEY HASH BASE64 16:", key16);

                Log.e("MY KEY HASH BASE64 hex 32:", byteArrayToHex(get32KeyByteArray()));
                Log.e("MY KEY HASH BASE64 hex 16:", byteArrayToHex(get16KeyByteArray()));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

//
//66d5f8110bad7d82e59ec00349c7c18c9048b7c90a9960fdbf52993f8f331d37
//e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855

    public static void hideSystemUI(View decorView) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public static View.OnClickListener hideKeyboardListener() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        };


    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean isLogin) {
        this.isLogin = isLogin;

        if (loginStatusListener != null)
            loginStatusListener.OnLoginStatus(isLogin);
    }

    private OnLoginStatusListener loginStatusListener = null;


    public interface OnLoginStatusListener {
        public void OnLoginStatus(boolean isLogin);
    }

    public void setOnLoginStatusListener(OnLoginStatusListener listener) {
        loginStatusListener = listener;
    }

    public String getFCMToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    public static byte[] get16KeyByteArray(){
        return Base64.decode(key16,Base64.URL_SAFE);
    }

    public static byte[] get32KeyByteArray(){
        return Base64.decode(key32,Base64.URL_SAFE);
    }
}