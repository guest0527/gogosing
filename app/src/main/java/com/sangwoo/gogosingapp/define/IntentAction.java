package com.sangwoo.gogosingapp.define;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class IntentAction {

    private static final String PREFIX = "com.sangwoo.gogosingApp.define";

    public static final String EXTRA_STRING_KAKAO_ID =PREFIX+"EXTRA_STRING_KAKAO_ID";
    public static final String EXTRA_STRING_FACEBOOK_ID =PREFIX+"EXTRA_STRING_FACEBOOK_ID";

    public static final String EXTRA_STRING_ROAD_ADDRESS =PREFIX+"EXTRA_STRING_ROAD_ADDRESS";
    public static final String EXTRA_STRING_JIBUN_ADDRESS =PREFIX+"EXTRA_STRING_JIBUN_ADDRESS";

    public static final String EXTRA_STRING_DETAIL_TITLE = PREFIX+"EXTRA_STRING_DETAIL_TITLE";
    public static final String EXTRA_STRING_DETAIL_DESCRIPTION = PREFIX+"EXTRA+STRING_DETAIL_DESCRIPTION";
    public static final String EXTRA_INT_DETAIL_DESCRIPTION_IMAGE_RES_ID = PREFIX+"EXTRA_INT_DETAIL_DESCRIPTION_IMAGE_RES_ID";
}
