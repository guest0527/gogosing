package com.sangwoo.gogosingapp.define;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class JsonResultCodeDefine {
    public static final int CODE_SEARCH_ADDRESS = -1

            ;
    public static final int CODE_LGOIN_OK = 10000;
    public static final int CODE_MEMBER_LOGIN_FAIL = CODE_LGOIN_OK + 1;
    public static final int CODE_KAKAO_SOCIAL_LOGIN_FAIL = CODE_MEMBER_LOGIN_FAIL + 1;
    public static final int CODE_FACEBOOK_SOCIAL_LOGIN_FAIL = CODE_KAKAO_SOCIAL_LOGIN_FAIL + 1;
    public static final int CODE_JOIN_OK = CODE_FACEBOOK_SOCIAL_LOGIN_FAIL + 1;
    public static final int CODE_JOIN_FAIL = CODE_JOIN_OK + 1;
    public static final int CODE_DUPLICATE_ID_CHECK_OK = CODE_JOIN_FAIL + 1;
    public static final int CODE_DUPLICATE_ID_CHECK_FAIL = CODE_DUPLICATE_ID_CHECK_OK + 1;
    public static final int CODE_GET_USERINFO_FAIL = CODE_DUPLICATE_ID_CHECK_FAIL+1;
    public static final int CODE_GET_USERINFO_OK = CODE_GET_USERINFO_FAIL+1;
    public static final int CODE_MAKE_ORDER_OK = CODE_GET_USERINFO_OK+1;
    public static final int CODE_MAKE_ORDER_FAIL = CODE_MAKE_ORDER_OK +1;
    public static final int CODE_LOGOUT_OK = CODE_MAKE_ORDER_FAIL +1;
    public static final int CODE_MODIFY_OK = CODE_LOGOUT_OK+1;
    public static final int CODE_MODIFY_FAIL = CODE_MODIFY_OK+1;
    public static final int CODE_GET_ORDER_LIST_OK = CODE_MODIFY_FAIL+1;
    public static final int CODE_GET_ORDER_LIST_FAIL = CODE_GET_ORDER_LIST_OK+1;
    public static final int CODE_GET_NOTICE_OK = CODE_GET_ORDER_LIST_FAIL+1;
    public static final int CODE_GET_NOTICE_FAIL = CODE_GET_NOTICE_OK+1;

}