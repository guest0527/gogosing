package com.sangwoo.gogosingapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.JusoUtil.JusoData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SangWoo on 2016. 8. 7..
 */
public class SearchAddressListAdapter extends BaseAdapter {

    private List<JusoData> list = new ArrayList<>();

    public void listChange(List<JusoData> list) {
        this.list = list;
    }

    public void add(JusoData data) {
        list.add(data);
    }

    public void addAll(List<JusoData> list) {
        list.addAll(list);
    }

    public void clear() {
        list.clear();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public JusoData getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_search_address, null);
        }

        TextView zipCode = (TextView) view.findViewById(R.id.textview_zipcode);
        TextView jibun = (TextView) view.findViewById(R.id.textview_jibun_address);
        TextView road = (TextView) view.findViewById(R.id.textview_road_address);

        JusoData data = getItem(i);

        zipCode.setText(data.getZipCode());
        jibun.setText(data.getJibunAddres());
        road.setText(data.getRoadAddress());

        return view;
    }
}
