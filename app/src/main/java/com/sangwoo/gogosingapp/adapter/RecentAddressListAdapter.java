package com.sangwoo.gogosingapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.UserDataInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SangWoo on 2016. 8. 7..
 */
public class RecentAddressListAdapter extends BaseAdapter {

    public RecentAddressListAdapter(){
    }

    private List<UserDataInfo.RecentAddressInfo> list = new ArrayList<>();

    public void add(UserDataInfo.RecentAddressInfo data) {
        list.add(data);
    }


    public void clear() {
        list.clear();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public UserDataInfo.RecentAddressInfo getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_recent_address, null);
        }


        TextView jibun = (TextView) view.findViewById(R.id.textview_jibun_address);
        TextView road = (TextView) view.findViewById(R.id.textview_road_address);
        TextView detail = (TextView) view.findViewById(R.id.textview_detail_address);

        UserDataInfo.RecentAddressInfo info = getItem(i);

        jibun.setText(info.getJibunAddress());
        road.setText(info.getRoadAddress());
        detail.setText(info.getDetailAddress());


        return view;
    }
}
