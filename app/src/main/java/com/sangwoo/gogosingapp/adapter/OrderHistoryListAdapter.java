package com.sangwoo.gogosingapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.GetOrderDataInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SangWoo on 2016. 8. 7..
 */
public class OrderHistoryListAdapter extends BaseAdapter {

    private List<GetOrderDataInfo.OrderDetailInfo> list = new ArrayList<>();


    public void add(GetOrderDataInfo.OrderDetailInfo data) {
        list.add(data);
    }


    public void clear() {
        list.clear();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public GetOrderDataInfo.OrderDetailInfo getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_order_history, null);
        }
        GetOrderDataInfo.OrderDetailInfo data = getItem(i);


        TextView orderTime = (TextView) view.findViewById(R.id.textview_order_time);
        TextView reservationTime = (TextView) view.findViewById(R.id.textview_order_reservation_time);
        TextView orderInfo = (TextView) view.findViewById(R.id.textview_order);
        TextView orderAddress = (TextView) view.findViewById(R.id.textview_order_address);
        TextView orderPhone = (TextView) view.findViewById(R.id.textview_order_phone);
        TextView orderStatus = (TextView) view.findViewById(R.id.textview_order_status);

        orderTime.setText(data.getOrderTime());
        reservationTime.setText(data.getReservationTime());
        orderInfo.setText(data.getOrderInformation());
        orderAddress.setText(data.getOrderFullAddress());
        orderPhone.setText(data.getOrderPhone());
        orderStatus.setText(data.getOrderState());



        return view;
    }
}
