package com.sangwoo.gogosingapp.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.widget.Toast;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.Util.Encrypt.AES256Cipher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by SangWoo on 2016. 8. 20..
 */
public class Util {

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern VALID_PASSWOLD_REGEX_ALPHA_NUM = Pattern.compile("^[a-zA-Z0-9!@.#$%^&*?_~]{8,20}$"); // 8자리 ~ 20자리까지 가능

    private static String SHARED_KEY = "com.sangwoo.gogosingapp.shared";

    public static String SHARED_INSTAILL_TIME = "shared_install_time";

    public static String getDevicesUUID() {

        Context mContext = GogosingApp.getContext();

        final String installTime, androidId;
        installTime = "" + getStringShared(SHARED_INSTAILL_TIME);
        androidId = "" + android.provider.Settings.Secure.getString(mContext.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), installTime.hashCode());
        String deviceId = deviceUuid.toString();

        return deviceId;
    }

    public static void showShortToast(String str) {
        Toast.makeText(GogosingApp.getContext(), str, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(String str) {
        Toast.makeText(GogosingApp.getContext(), str, Toast.LENGTH_LONG).show();
    }


    public static void setStringShared(String key, String value) {
        SharedPreferences pref = GogosingApp.getContext().getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringShared(String key) {
        SharedPreferences pref = GogosingApp.getContext().getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE);
        return pref.getString(key, null);
    }

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean validatePassword(String pwStr) {
        Matcher matcher = VALID_PASSWOLD_REGEX_ALPHA_NUM.matcher(pwStr);
        return matcher.matches();
    }

    public static String encode_aes(String str){
        try {
            return AES256Cipher.AES_Encode(str);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getReadFile(Context context, String fileName){

        AssetManager assetManager = context.getResources().getAssets();

        StringBuilder sb = new StringBuilder();

        try{
            AssetManager.AssetInputStream assetInputStream = (AssetManager.AssetInputStream) assetManager.open(fileName);

            BufferedReader br = new BufferedReader(new InputStreamReader(assetInputStream));
            // InputStreamReader is = new InputStreamReader(assetInputStream);

            int bufferSize = 1024 * 1024 * 1;

            int resultSize;

            char[] readBuf = new char[bufferSize];

            while((resultSize = br.read(readBuf)) != -1){
                if(resultSize == bufferSize){
                    sb.append(readBuf);
                }else{
                    for(int i = 0; i < resultSize; i++){
                        sb.append(readBuf[i]);
                    }
                }

            }
        }catch(IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return sb.toString();
    }
}
