package com.sangwoo.gogosingapp.Util.ServerUtil;

import com.sangwoo.gogosingapp.Util.Encrypt.AES256Cipher;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by SangWoo on 2016. 8. 19..
 */
public class ModifyInfo extends Info {
    private final static String PARAM_PASSWD = "pw";
    private final static String PARAM_CHANGE_PASSWD = "changepw";
    private final static String PARAM_TEL = "tel";
    private final static String PARAM_JIBUN_ADDRESS = "jibunAddress";
    private final static String PARAM_ROAD_ADDRESS = "roadAddress";
    private final static String PARAM_DETAIL_ADDRESS = "detailAddress";
    private final static String PARAM_BIRTH = "brith";

    public void setPassword(String password) throws NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        map.put(PARAM_PASSWD, AES256Cipher.AES_Encode(password));
    }

    public void setChangePassword(String password) throws NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        map.put(PARAM_CHANGE_PASSWD,AES256Cipher.AES_Encode(password));
    }

    public void setPhone(String tel) {
        map.put(PARAM_TEL, tel);
    }

    public void setJibunAddress(String jibunAddress) {
        map.put(PARAM_JIBUN_ADDRESS, jibunAddress);
    }

    public void setRoadAddress(String roadAddress) {
        map.put(PARAM_ROAD_ADDRESS, roadAddress);
    }

    public void setDetailAddress(String detailAddress) {
        map.put(PARAM_DETAIL_ADDRESS, detailAddress);
    }

    public void setBirth(String birth) {
        map.put(PARAM_BIRTH, birth);
    }


}
