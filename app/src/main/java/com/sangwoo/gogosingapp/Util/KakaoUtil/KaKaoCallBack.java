package com.sangwoo.gogosingapp.Util.KakaoUtil;

import android.util.Log;

import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.usermgmt.UserManagement;
import com.kakao.util.exception.KakaoException;

/**
 * Created by SangWoo on 2016. 7. 23..
 */
public class KaKaoCallBack implements ISessionCallback{
    @Override
    public void onSessionOpened() {
//        UserManagement.requestMe();
        //login ok
        UserManagement.requestMe(new KakaoRequestCallback());
    }

    @Override
    public void onSessionOpenFailed(KakaoException exception) {
        //login fail

        if(exception != null)
            exception.printStackTrace();
    }
}
