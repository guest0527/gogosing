package com.sangwoo.gogosingapp.Util.KakaoUtil;

import android.app.Activity;
import android.content.Context;

import com.kakao.auth.IApplicationConfig;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.ISessionConfig;
import com.kakao.auth.KakaoAdapter;
import com.sangwoo.gogosingapp.GogosingApp;

/**
 * Created by SangWoo on 2016. 7. 23..
 */
public class KaKaoSDKApapter extends KakaoAdapter {

    @Override
    public IApplicationConfig getApplicationConfig() {
        return new KaKaoApplicationConfig();
    }

    @Override
    public ISessionConfig getSessionConfig() {
        return new KakaoSessionConfig();
    }
}
