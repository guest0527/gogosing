package com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack;

import android.os.Message;
import android.util.Log;

import com.sangwoo.gogosingapp.Util.JusoUtil.JusoCommonData;
import com.sangwoo.gogosingapp.Util.JusoUtil.JusoData;
import com.sangwoo.gogosingapp.Util.JusoUtil.JusoVO;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultCallBack;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class JusoResult extends ServerResultCallBack{

    private JusoVO vo = new JusoVO();

    @Override
    public void onServerResult() {
        String result = getResult();
        SAXParserFactory saxPF = SAXParserFactory.newInstance();
        SAXParser saxParser = null;
        InputStream is = null;

        try {
            is = new ByteArrayInputStream(result.getBytes("UTF-8"));

            InputSource inputSource = new InputSource(is);
            saxParser = saxPF.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();

            xmlReader.setContentHandler(vo.getHandler());


            xmlReader.parse(inputSource);

            if(getHandler() != null) {
                Message msg = getHandler().obtainMessage();
                msg.what = JsonResultCodeDefine.CODE_SEARCH_ADDRESS;
                msg.obj = vo;
                getHandler().sendMessage(msg);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
