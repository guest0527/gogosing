package com.sangwoo.gogosingapp.Util.FBUtil;

import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;

/**
 * Created by SangWoo on 2016. 7. 22..
 */
public class FBTokenTracker extends AccessTokenTracker {

    @Override
    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
        if (oldAccessToken != null)
            logoff(oldAccessToken);
        if (currentAccessToken != null)
            logIn(currentAccessToken);
    }

    private void logoff(AccessToken token) {
    }

    private void logIn(AccessToken token) {
    }
}
