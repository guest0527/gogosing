package com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo;

import com.sangwoo.gogosingapp.adapter.RecentAddressListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by SangWoo on 2016. 8. 19..
 */
public class UserDataInfo {


    private final static String MEMBER_EMAIL ="email";
    private final static String MEMBER_NUMBER = "member_number";
    private final static String NAME = "name";
    private final static String TEL = "tel";
    private final static String JIBUN_ADDRESS = "jibunAddress";
    private final static String ROAD_ADDRESS = "roadAddress";
    private final static String DETAIL_ADDRESS = "detailAddress";
    private final static String BIRTH = "birth";
    private final static String POINT = "point";
    private final static String RECENT_LIST = "addressList";

    private JSONObject jsonObject;

    public UserDataInfo(JSONObject jsonObject) {
        if (jsonObject == null)
            jsonObject = new JSONObject();

        this.jsonObject = jsonObject;
    }

    public String getEmail(){
        return jsonObject.optString(MEMBER_EMAIL);
    }

    public String getID() {
        return jsonObject.optString(MEMBER_NUMBER);
    }

    public String getName() {
        return jsonObject.optString(NAME);
    }

    public String getPhone() {
        return jsonObject.optString(TEL);
    }

    public String getRoadAddress() {
        return jsonObject.optString(ROAD_ADDRESS);
    }

    public String getJibunAddress() {
        return jsonObject.optString(JIBUN_ADDRESS);
    }

    public String getDetailAddress() {
        return jsonObject.optString(DETAIL_ADDRESS);
    }

    public String getSpcialDay() {
        String birth = jsonObject.optString(BIRTH);
        if(birth.equals("null"))
            birth = null;
        return birth;
    }

    public int getPoint() {
        return jsonObject.optInt(POINT);
    }

    public int getRecentAddressListSize(){
        JSONArray array = jsonObject.optJSONArray(RECENT_LIST);

        if(array == null)
            return 0;
        return array.length();
    }

    public RecentAddressInfo getRecentAddress(int index){
        JSONArray array = jsonObject.optJSONArray(RECENT_LIST);

        if(array == null)
            return null;
        return new RecentAddressInfo(array.optJSONObject(index));
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }

    public class RecentAddressInfo{

        JSONObject jsonObject = null;

        public RecentAddressInfo(JSONObject jsonObject){
            if(jsonObject == null)
                jsonObject =new JSONObject();
            this.jsonObject = jsonObject;
        }

        public String getJibunAddress(){
            return jsonObject.optString(JIBUN_ADDRESS);
        }

        public String getRoadAddress(){
            return jsonObject.optString(ROAD_ADDRESS);
        }

        public String getDetailAddress(){
            return jsonObject.optString(DETAIL_ADDRESS);
        }
    }
}
