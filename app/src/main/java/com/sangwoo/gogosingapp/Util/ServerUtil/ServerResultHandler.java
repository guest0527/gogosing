package com.sangwoo.gogosingapp.Util.ServerUtil;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.sangwoo.gogosingapp.dialog.WaitDialog;

/**
 * Created by SangWoo on 2016. 9. 22..
 */
public class ServerResultHandler extends Handler {

    private Activity activity = null;
    private WaitDialog dialog = null;
    private boolean isShowDialog = true;

    public ServerResultHandler(Activity activity){
        this.activity = activity;
        dialog = new WaitDialog(activity);
    }

    public ServerResultHandler(Activity activity,boolean isShowDialog){
        this.activity = activity;
        dialog = new WaitDialog(activity);
        this.isShowDialog = isShowDialog;
    }

    @Override
    public void handleMessage(Message msg) {
        if(isShowDialog == false)
            return;

        int what = msg.what;

        switch (what){
            case ServerResultCallBack.HTTP_WAIT:
                dialog.show();
                break;
            case ServerResultCallBack.HTTP_RESULT_OK :
                dialog.dismiss();
                break;
            default:
                break;
        }
    }
}
