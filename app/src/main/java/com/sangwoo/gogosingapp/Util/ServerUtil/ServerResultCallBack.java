package com.sangwoo.gogosingapp.Util.ServerUtil;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public abstract class ServerResultCallBack implements IServerResult{

    public final static int HTTP_WAIT = 5000;
    public final static int HTTP_RESULT_OK = HTTP_WAIT+1;
    private Message msg = null;
    private Handler handler;
    private String result;
    private Map<String, String> map = null;

    final public void setHandler(Handler handler){
        this.handler = handler;
        msg = Message.obtain();
    }

    final public Handler getHandler(){
        return handler;
    }

    final public Message getMsg(){
        if(msg == null){
            msg = Message.obtain();
        }
        return msg;
    }

    final public void sendMsg() {
        if(getHandler() != null){
            getHandler().sendMessage(msg);
        }
    }


    final public void saveResult(String result) {
        this.result = result;
    }

    final public String getResult(){
        return this.result;
    }

    final public JSONObject getJsonResult(){

        try {
            return new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  null;
    }

    final private void sendMsg(Message msg) {
        if(getHandler() != null){
            getHandler().sendMessage(msg);
        }
    }

    final public void onHttpWaitMessage(){
        Message msg = Message.obtain();
        msg.what = HTTP_WAIT;
        sendMsg(msg);
    }

//    final public void on

    final public void onHttpResultOkMessage(){
        Message msg = Message.obtain();
        msg.what = HTTP_RESULT_OK;
        sendMsg(msg);
    }



    final public void saveParam(Map<String,String> map){
        this.map = map;
    }

    final public Map<String,String> getParam(){
        return  map;
    }
}
