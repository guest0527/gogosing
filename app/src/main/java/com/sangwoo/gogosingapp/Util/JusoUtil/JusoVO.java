package com.sangwoo.gogosingapp.Util.JusoUtil;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SangWoo on 2016. 7. 25..
 */
public class JusoVO extends DefaultHandler {

    private boolean isStart = false;
    private String value = null;

    private static final String XML_KEY_COMMON = "common";
    private static final String XML_KEY_JUSO = "juso";

    private static final String XML_KEY_TOTAL_COUNT = "totalCount";
    private static final String XML_KEY_CURRENT_PAGE = "currentPage";
    private static final String XML_KEY_COUNT_PER_PAGE = "countPerPage";
    private static final String XML_KEY_ERROR_CODE = "errorCode";
    private static final String XML_KEY_ERROR_MESSAGE = "errorMessage";
    private static final String XML_KEY_ROAD_ADDRESS = "roadAddr";
    private static final String XML_KEY_JIBUN_ADDRESS = "jibunAddr";
    private static final String XML_KEY_ZIP_CODE = "zipNo";

    private JusoCommonData commonData = null;
    private List<JusoData> jusoDataList = new ArrayList<>();
    private JusoData jusoData = null;

    public DefaultHandler getHandler() {
        return (DefaultHandler) this;
    }


    public JusoCommonData getCommonData() {
        return commonData;
    }

    public JusoData getJusoData(int index) {
        return jusoDataList.get(index);
    }

    public List<JusoData> getJusoDataList(){
        List list = new ArrayList();
        list.addAll(jusoDataList);
        return list;
    }

    public int getJusoDataSize() {
        return jusoDataList.size();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        isStart = true;

        if (localName.equalsIgnoreCase(XML_KEY_COMMON)) {
            commonData = new JusoCommonData();
        } else if (localName.equalsIgnoreCase(XML_KEY_JUSO)) {
            jusoData = new JusoData();
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        isStart = false;
        if (localName.equalsIgnoreCase(XML_KEY_TOTAL_COUNT)) {
            commonData.setTotalCount(Integer.valueOf(value));
        } else if (localName.equalsIgnoreCase((XML_KEY_CURRENT_PAGE))) {
            commonData.setCurrentPage(Integer.valueOf(value));
        } else if (localName.equalsIgnoreCase(XML_KEY_COUNT_PER_PAGE)) {
            commonData.setCountPerPage(Integer.valueOf(value));
        } else if (localName.equalsIgnoreCase(XML_KEY_ERROR_CODE)) {
            commonData.setErrorCode(Integer.valueOf(value));
        } else if (localName.equalsIgnoreCase(XML_KEY_ERROR_MESSAGE)) {
            commonData.setErrorMessage(value);
        } else if (localName.equalsIgnoreCase(XML_KEY_ROAD_ADDRESS)) {
            jusoData.setRoadAddress(value);
        } else if (localName.equalsIgnoreCase(XML_KEY_JIBUN_ADDRESS)) {
            jusoData.setJibunAddres(value);
        } else if (localName.equalsIgnoreCase(XML_KEY_ZIP_CODE)) {
            jusoData.setZipCode(value);
        } else if (localName.equalsIgnoreCase(XML_KEY_JUSO)) {
            jusoDataList.add(jusoData);
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (isStart) {
            value = new String(ch, start, length);
        }
    }


}
