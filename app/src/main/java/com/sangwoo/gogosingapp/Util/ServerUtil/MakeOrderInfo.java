package com.sangwoo.gogosingapp.Util.ServerUtil;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.Util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by SangWoo on 2016. 8. 19..
 */
public class MakeOrderInfo extends Info {
    //주문내용/지번주소/도로명주소/상세주소/연락처/예약시간/결제수단/기타요청사항

    public enum PAYMENT_METHOD {

        CARD(1), CASH(2), BANK(3);
        private int idx = 0;

        private PAYMENT_METHOD(int idx) {
            this.idx = idx;
        }

        public int getVal() {
            return idx;
        }
    }

    private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static String PARAM_MEMBER_ID = "member_number";
    private final static String PARAM_ORDER = "order";
    private final static String PARAM_JIBUN_ADDRESS = "jibunAddress";
    private final static String PARAM_ROAD_ADDRESS = "roadAddress";
    private final static String PARAM_DETAIL_ADDRESS = "detailAddress";
    private final static String PARAM_TEL = "tel";
    private final static String PARAM_RESERVATION_TIME = "reservation_time";
    private final static String PARAM_PAYMENT_METHOD = "payment_method";
    private final static String PARAM_ORDER_COMMENT = "order_comment";
    private final static String PARAM_DEVICE_UUID = "uuid";
    private final static String PARAM_FCM_TOKEN = "fcmToken";


    public String getReservationTime() {
        return map.get(PARAM_RESERVATION_TIME);
    }

    public String getRoadAddress() {
        return map.get(PARAM_ROAD_ADDRESS);
    }

    public String getJibunAddress() {
        return map.get(PARAM_JIBUN_ADDRESS);
    }

    public String getDetailAddress(){
        return map.get(PARAM_DETAIL_ADDRESS);
    }

    public String getPhone() {
        return map.get(PARAM_TEL);
    }


    public MakeOrderInfo() {
        map.put(PARAM_FCM_TOKEN, GogosingApp.getApp().getFCMToken());
        map.put(PARAM_DEVICE_UUID, Util.getDevicesUUID());
    }

    public void setOrder(String order) {
        map.put(PARAM_ORDER, order);
    }

    public void setJibunAddress(String jibunAddress) {
        map.put(PARAM_JIBUN_ADDRESS, jibunAddress);
    }

    public void setRoadAddress(String roadAddress) {
        map.put(PARAM_ROAD_ADDRESS, roadAddress);
    }

    public void setDetailAddress(String detailAddress) {
        map.put(PARAM_DETAIL_ADDRESS, detailAddress);
    }

    public void setPhone(String tel) {
        map.put(PARAM_TEL, tel);
    }

    public void setReservationTime(Date timeDate) {
        //simpledataFormat 이용해서 스트링 변환해서 넣을것
        if(timeDate == null)
            return;
        map.put(PARAM_RESERVATION_TIME, FORMAT.format(timeDate).toString());
    }

    public void setPaymentMethod(PAYMENT_METHOD paymentMethod) {
        map.put(PARAM_PAYMENT_METHOD, String.valueOf(paymentMethod.getVal()));
    }

    public void setID(String id){
        map.put(PARAM_MEMBER_ID,id);
    }

    public void setOrderComment(String comment) {
        map.put(PARAM_ORDER_COMMENT, comment);
    }

    @Override
    Map<String, String> getMap() {
        return super.getMap();
    }
}
