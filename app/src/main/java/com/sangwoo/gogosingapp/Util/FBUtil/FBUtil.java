package com.sangwoo.gogosingapp.Util.FBUtil;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;

/**
 * Created by SangWoo on 2016. 7. 23..
 */
public class FBUtil {

    private CallbackManager callbackManager = null;
    private FBTokenTracker fbTokenTracker = null;
    private FBProfileTracker fbProfileTracker = null;
    private FacebookCallback<LoginResult> fbLoginResultCallback = null;

    public FacebookCallback<LoginResult> getLoginResultCallback(){
        if(fbLoginResultCallback == null)
            fbLoginResultCallback = new FBCallBack();
        return fbLoginResultCallback;
    }


    public CallbackManager getCallbackManager(){
        if(callbackManager == null)
            callbackManager = CallbackManager.Factory.create();
        return callbackManager;
    }

    public FBTokenTracker getTokenTracker(){
        if(fbTokenTracker == null)
            fbTokenTracker = new FBTokenTracker();
        return fbTokenTracker;
    }

    public FBProfileTracker getProfileTracker(){
        if(fbProfileTracker == null)
            fbProfileTracker = new FBProfileTracker();
        return fbProfileTracker;
    }

    public void stopTokenTracker(){
        if(fbTokenTracker != null)
            fbTokenTracker.stopTracking();
    }

    public void stopProfileTracker(){
        if(fbProfileTracker != null)
            fbProfileTracker.stopTracking();
    }
}
