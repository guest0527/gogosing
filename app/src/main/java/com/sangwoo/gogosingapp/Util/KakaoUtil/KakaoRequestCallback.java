package com.sangwoo.gogosingapp.Util.KakaoUtil;

import android.util.Log;

import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.sangwoo.gogosingapp.GogosingApp;

/**
 * Created by SangWoo on 2016. 7. 24..
 */
public class KakaoRequestCallback extends MeResponseCallback {
    @Override
    public void onSessionClosed(ErrorResult errorResult) {
        //session close
    }

    @Override
    public void onNotSignedUp() {
        //login ok
        //카카오톡 회원가입 필요
    }

    @Override
    public void onSuccess(UserProfile result) {
        //로그인 완료

        //handler setting 하기.로그인 결과값으로 페이지 이동 결정
//        GogosingApp.getApp().getServerUtil().setLoginHandler();
        GogosingApp.getApp().getServerUtil().kakaoLogin(String.valueOf(result.getId()));
    }

    @Override
    public void onFailure(ErrorResult errorResult) {
    }
}
