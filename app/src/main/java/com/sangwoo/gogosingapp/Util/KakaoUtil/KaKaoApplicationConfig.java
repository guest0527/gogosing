package com.sangwoo.gogosingapp.Util.KakaoUtil;

import android.app.Activity;
import android.content.Context;

import com.kakao.auth.IApplicationConfig;
import com.sangwoo.gogosingapp.GogosingApp;

/**
 * Created by SangWoo on 2016. 7. 24..
 */
public class KaKaoApplicationConfig implements IApplicationConfig {

    @Override
    public Activity getTopActivity() {
        return GogosingApp.getCurrentActivity();
    }

    @Override
    public Context getApplicationContext() {
        return GogosingApp.getContext();
    }
}
