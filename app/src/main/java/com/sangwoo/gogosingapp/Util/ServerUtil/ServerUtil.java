package com.sangwoo.gogosingapp.Util.ServerUtil;

import android.os.Handler;
import android.util.Log;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.Util.Encrypt.AES256Cipher;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.DefaultResultCallBack;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.JusoResult;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.NoticeResult;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.OrderHistoryResult;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.UserInfoResult;
import com.sangwoo.gogosingapp.Util.Util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by SangWoo on 2016. 7. 22..
 */
public class ServerUtil {
    private static final String SERVER_URL = "http://52.78.69.106/";

    public static final String API_GET_ORDER_LIST = SERVER_URL + "order/getOrder.php";
    public static final String API_MAKE_ORDER = SERVER_URL + "order/makeOrder.php";
    public static final String API_MEMBER_JOIN = SERVER_URL +    "member/join.php";
    public static final String API_MEMBER_MODIFY = SERVER_URL +    "member/modify.php";
    public static final String API_MEMBER_LOGIN = SERVER_URL + "member/login.php";
    public static final String API_MEMBER_LOGOUT = SERVER_URL + "member/logout.php";
    public static final String API_SEARCH_JUSO = SERVER_URL + "lib/searchJuso.php";
    public static final String API_DUPLICATE_EMAIL_CHECK = SERVER_URL + "member/duplicateIDChecker.php";
    public static final String API_GET_USERINFO = SERVER_URL + "member/userinfo.php";
    public static final String API_GET_NOTICE = SERVER_URL + "notice/notice.php";


    public static final String JSON_RESULT = "result";
    public static final String JSON_DATA = "data";
    public static final String JSON_ERROR = "error";
    public static final String JSON_OK = "sucess";
    public static final String JSON_RESULT_CODE = "code";
    public static final String JSON_MSG = "msg";

    public static final String PARAM_KAKAO_ID = "kakao_id";
    public static final String PARAM_FACEBOOK_ID = "facebook_id";
    public static final String PARAM_SITE_EMAIL = "email";
    public static final String PARAM_SITE_PW = "pw";
    public static final String PARAM_SEARCH_JUSO = "search";
    public static final String PARAM_RECENT_ORDER_SIZE="recent_order_size";
    public static final String PARAM_UUID = "uuid";

    private static String RECENT_ORDER_SIZE = "3";

    private static String cookie;

    private Handler loginHandler = null;

    public void serverSendTest() {
        serverSend(SERVER_URL + "order/getOrder.php", null, null);
    }

    public void getRecentOrder(Handler handler){
        Map<String, String> map = new HashMap<>();
        map.put(PARAM_RECENT_ORDER_SIZE, RECENT_ORDER_SIZE);
        map.put(PARAM_UUID, Util.getDevicesUUID());
        serverSend(API_GET_ORDER_LIST,map,new OrderHistoryResult(handler));
    }


    public void updateUserInfo(ModifyInfo info, Handler handler){
        serverSend(API_MEMBER_MODIFY,info.getMap(),new DefaultResultCallBack(handler));
    }

    public void setLoginHandler(Handler handler) {
        loginHandler = handler;
    }

    public void kakaoLogin(String kakaoID) {
        serverLogin(makeMap(PARAM_KAKAO_ID, kakaoID));
    }

    public void getUserInfo(Handler handler){
        serverSend(API_GET_USERINFO, null, new UserInfoResult(handler));
    }

    public void siteLogout(){
        serverSend(API_MEMBER_LOGOUT ,null,null);
        cookie = null;
        GogosingApp.getApp().setLogin(false);
    }

    public void getNotice(Handler handler){
        serverSend(API_GET_NOTICE, null, new NoticeResult(handler));
    }


    public void facebookLogin(String facebookID) {
        serverLogin(makeMap(PARAM_FACEBOOK_ID, facebookID));
    }

    public void sendOrder(MakeOrderInfo info, Handler handler){
        serverSend(API_MAKE_ORDER, info.getMap(), new DefaultResultCallBack(handler));
    }

    public void siteLogin(String email, String password) throws NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Map<String, String> map = new HashMap<>();
        map.put(PARAM_SITE_EMAIL, email);
        map.put(PARAM_SITE_PW, AES256Cipher.AES_Encode(password));
        serverLogin(map);
    }


    public void siteJoin(JoinInfo info, Handler handler) {
        serverSend(API_MEMBER_JOIN, info.getMap(), new DefaultResultCallBack(handler));
    }

    public void duplicateEmailCheck(String email, Handler handler) {
        Log.d("ServerUtil", "duplcate ID " + email);
        ServerResultCallBack callBack = new DefaultResultCallBack();
        callBack.setHandler(handler);
        serverSend(API_DUPLICATE_EMAIL_CHECK, makeMap(PARAM_SITE_EMAIL, email), callBack);
    }


    private void serverLogin(final Map<String, String> map) {
        ServerResultCallBack result = new DefaultResultCallBack();
        result.setHandler(loginHandler);
        serverSend(API_MEMBER_LOGIN, map, result);
    }

    public void searchJuso(String search, Handler handler) {
        JusoResult result = new JusoResult();
        search = search.replace(" ","");
        result.setHandler(handler);
        serverSend(API_SEARCH_JUSO, makeMap(PARAM_SEARCH_JUSO, search), result);
    }

    private Map<String, String> makeMap(String key, String val) {
        Map<String, String> map = new HashMap<>();
        map.put(key, val);
        return map;
    }

    private String getParam(final Map<String, String> map){
        String sb = null;
        if (map != null && !map.isEmpty()) {
            Iterator<String> it = map.keySet().iterator();
            boolean isFirst = true;
            while (it.hasNext()) {
                String key = it.next();
                String val = map.get(key);


                if (!isFirst)
                    sb = sb + "&";

                if (isFirst) {
                    sb = encode(key) + "=" + encode(val);
                } else {
                    sb = sb + encode(key) + "=" + encode(val);
                }

                isFirst = false;
            }
        }

        return sb;
    }


    private void serverSend(final String targetURL, final Map<String, String> map, final ServerResultCallBack result) {

        final String parameter = getParam(map);

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {

                    String stringUrl = targetURL;

                    if (parameter != null) {
                        stringUrl = stringUrl + "?" + parameter;
                    }
                    URL url = new URL(stringUrl);

                    if (result != null) {
                        result.onHttpWaitMessage();
                    }


                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();


                    if(cookie != null) {
                        conn.setRequestProperty("Cookie", cookie);
                    }
                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);


                    if (cookie == null) {

                        List<String> list = conn.getHeaderFields().get("Set-Cookie");
                        if (list != null) {
                            for (int i = 0; i < list.size();i++) {
                                if(cookie == null)
                                    cookie = list.get(i);
                                else
                                    cookie = cookie+list.get(i);
                            }
                        }
                    }

                    InputStream is = conn.getInputStream();
                    byte[] byteBuffer = new byte[1024];
                    byte[] byteData = null;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    int length = 0;
                    while ((length = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                        baos.write(byteBuffer, 0, length);
                    }


                    byteData = baos.toByteArray();

                    if (result != null) {
                        result.saveParam(map);
                        result.saveResult(new String(byteData));
                        result.onServerResult();
                        result.onHttpResultOkMessage();
                    }
                } catch (ConnectException e) {
                    //popup 등 시나리오 작성
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private String encode(String str) {
        if (str == null)
            return "";
        try {
            String result = URLEncoder.encode(str, "UTF-8");
            return result;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }
}