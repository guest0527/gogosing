package com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack;

import android.os.Handler;

import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultCallBack;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerUtil;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.UserDataInfo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class UserInfoResult extends ServerResultCallBack {


    public UserInfoResult(Handler handler){
        this.setHandler(handler);
    }


    @Override
    public void onServerResult() {



        JSONObject jsonObject = getJsonResult();
        JSONObject jsonResult = null;

        try {
            jsonResult = (JSONObject) jsonObject.get(ServerUtil.JSON_RESULT);

            int resultCode = jsonResult.optInt(ServerUtil.JSON_RESULT_CODE, -1);

            getMsg().what = resultCode;
            getMsg().obj = new UserDataInfo(jsonResult.getJSONObject(ServerUtil.JSON_DATA));

            sendMsg();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
