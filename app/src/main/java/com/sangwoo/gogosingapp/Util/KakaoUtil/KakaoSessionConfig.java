package com.sangwoo.gogosingapp.Util.KakaoUtil;

import com.kakao.auth.ApprovalType;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionConfig;

/**
 * Created by SangWoo on 2016. 7. 24..
 */
public class KakaoSessionConfig implements ISessionConfig{
    @Override
    public AuthType[] getAuthTypes() {
        return new AuthType[]{AuthType.KAKAO_LOGIN_ALL};
    }

    @Override
    public boolean isUsingWebviewTimer() {
        return false;
    }

    @Override
    public ApprovalType getApprovalType() {
        return ApprovalType.INDIVIDUAL;
    }

    @Override
    public boolean isSaveFormData() {
        return true;
    }
}
