package com.sangwoo.gogosingapp.Util.FBUtil;

import android.util.Log;

import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.sangwoo.gogosingapp.GogosingApp;

/**
 * Created by SangWoo on 2016. 7. 22..
 */
public class FBProfileTracker extends ProfileTracker {
    @Override
    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

        if(oldProfile != null) {
            logoff(oldProfile);
        }
        //로그인
        if(currentProfile != null) {
            logIn(currentProfile);
        }
    }

    private void logoff(Profile profile){
    }

    private void logIn(Profile profile){
        GogosingApp.getApp().getServerUtil().facebookLogin(profile.getId());
    }
}
