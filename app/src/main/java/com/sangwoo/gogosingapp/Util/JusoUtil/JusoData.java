package com.sangwoo.gogosingapp.Util.JusoUtil;

import android.util.Log;

/**
 * Created by SangWoo on 2016. 7. 26..
 */
public class JusoData {

    private String roadAddress;
    private String jibunAddres;
    private String zipCode;

    public void setRoadAddress(String roadAddress) {
        this.roadAddress = roadAddress;
    }

    public void setJibunAddres(String jibunAddres) {
        this.jibunAddres = jibunAddres;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getJibunAddres() {
        return jibunAddres;
    }

    public String getRoadAddress() {
        return roadAddress;
    }

    public String getZipCode() {
        return zipCode;
    }
}
