package com.sangwoo.gogosingapp.Util.ServerUtil;

import java.util.Map;

/**
 * Created by SangWoo on 2016. 7. 22..
 */
public interface IServerResult {
    public void onServerResult();

}
