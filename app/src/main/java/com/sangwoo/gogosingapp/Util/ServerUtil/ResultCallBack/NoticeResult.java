package com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack;

import android.os.Handler;

import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.GetOrderDataInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.NoticeInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultCallBack;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class NoticeResult extends ServerResultCallBack {


    public NoticeResult(Handler handler){
        this.setHandler(handler);
    }
    @Override
    public void onServerResult() {



        JSONObject jsonObject = getJsonResult();
        JSONObject jsonResult = null;

        try {
            jsonResult = (JSONObject) jsonObject.get(ServerUtil.JSON_RESULT);

            int resultCode = jsonResult.optInt(ServerUtil.JSON_RESULT_CODE, -1);

            getMsg().what = resultCode;
            getMsg().obj = new NoticeInfo(jsonResult.optJSONArray(ServerUtil.JSON_DATA));
            sendMsg();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
