package com.sangwoo.gogosingapp.Util.FBUtil;

import android.util.Log;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.dialog.OrderConfirmDialog;

/**
 * Created by SangWoo on 2016. 7. 22..
 */
public class FBCallBack implements FacebookCallback<LoginResult>{

    private LoginResult loginResult = null;

    public FBCallBack(){
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        // login sucess 처리
        GogosingApp.getApp().getServerUtil().facebookLogin(loginResult.getAccessToken().getUserId());
        this.loginResult = loginResult;
    }


    @Override
    public void onCancel() {
        if(loginResult != null){
            GogosingApp.getApp().getServerUtil().facebookLogin(loginResult.getAccessToken().getUserId());
        }
    }

    @Override
    public void onError(FacebookException error) {
        error.printStackTrace();
    }
}
