package com.sangwoo.gogosingapp.Util.KakaoUtil;

import android.os.Handler;
import android.util.Log;

import com.kakao.auth.ISessionCallback;
import com.kakao.auth.KakaoAdapter;
import com.kakao.auth.Session;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;

/**
 * Created by SangWoo on 2016. 7. 24..
 */
public class KakaoUtil {

    private ISessionCallback mCallback = null;
    private KakaoAdapter mAdapter = null;

    public ISessionCallback getCallBack(){
        if(mCallback == null)
            mCallback = new KaKaoCallBack();
        return mCallback;
    }

    public KakaoAdapter getAdapter(){
        if(mAdapter == null)
            mAdapter = new KaKaoSDKApapter();
        return mAdapter;
    }

    public Session getCurrentSession(){
        return Session.getCurrentSession();
    }

    public void logoff(){
        UserManagement.requestLogout(new LogoutResponseCallback() {
            @Override
            public void onCompleteLogout() {
            }
        });
    }
}
