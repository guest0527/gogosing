package com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo;

import android.util.Log;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

/**
 * Created by SangWoo on 2016. 8. 19..
 */
public class GetOrderDataInfo {

    private JSONArray jsonArray;


    public GetOrderDataInfo(JSONArray jsonArray){
        if(jsonArray == null)
            jsonArray = new JSONArray();

        this.jsonArray = jsonArray;
    }

    public int getOrderHistorySize(){
        return jsonArray.length();
    }

    public OrderDetailInfo getOrderDetailInfo(int index){
        return new OrderDetailInfo(jsonArray.optJSONObject(index));
    }


    @Override
    public String toString() {
        return jsonArray.toString();
    }

    public class OrderDetailInfo{

        private final static String ORDER_NUMBER = "order_number";
        private final static String ORDER_MEMBER_NUMBER = "order_member_number";
        private final static String ORDER_PHONE_NUMBER = "order_phone_number";
        private final static String ORDER_DELIVERY_JIBUN_ADDRESS = "delivery_jibunaddress";
        private final static String ORDER_DELIVERY_ROAD_ADDRESS = "delivery_roadaddress";
        private final static String ORDER_DELIVERY_DETAIL_ADDRESS = "delivery_detailaddress";
        private final static String ORDER_INFORMATION = "order_information";
        private final static String ORDER_COMMENT = "order_comment";
        private final static String ORDER_RESERVATION_TIME = "reservation_time";
        private final static String ORDER_CHECK_TIME ="order_check_time";
        private final static String ORDER_PAYMENT_METHOD = "payment_method";
        private final static String ORDER_STATE = "order_state";

        private JSONObject jsonObject = null;

        private OrderDetailInfo(JSONObject jsonObject){
            Log.d("SW_DEUBG", "OrderDetailInfo " + jsonObject.toString());
            if(jsonObject == null)
                jsonObject = new JSONObject();

            this.jsonObject = jsonObject;
        }

        //주문 시간/ 예약 시간 / 주문 내용 / 연락처 / 주소 /상태

        public String getOrderTime(){
            return jsonObject.optString(ORDER_NUMBER);
        }

        public String getReservationTime(){
            String orderTime = getOrderTime();
            String reservationTime = jsonObject.optString(ORDER_RESERVATION_TIME);
            return reservationTime.equals(orderTime) ?   GogosingApp.getContext().getString(R.string.string_order_page_reservation_time_right_away) : reservationTime;
        }

        public String getOrderInformation(){
            return jsonObject.optString(ORDER_INFORMATION);
        }

        public String getOrderPhone(){
            return jsonObject.optString(ORDER_PHONE_NUMBER);
        }

        public String getOrderFullAddress(){
            return jsonObject.optString(ORDER_DELIVERY_ROAD_ADDRESS)+" "+jsonObject.optString(ORDER_DELIVERY_DETAIL_ADDRESS);
        }

        public String getOrderState(){
            return jsonObject.optString(ORDER_STATE);
        }


    }
}
