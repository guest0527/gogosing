package com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo;

import org.json.JSONArray;

/**
 * Created by SangWoo on 2016. 12. 1..
 */
public class NoticeInfo {

    private static final String NOTICE_CONTENT = "content";

    private JSONArray jsonArray;


    public NoticeInfo(JSONArray jsonArray){
        if(jsonArray == null)
            jsonArray = new JSONArray();

        this.jsonArray = jsonArray;
    }

    public String getNotice(int index){
        return jsonArray.optJSONObject(index).optString(NOTICE_CONTENT);
    }

    public int getNoticeLength(){
        return jsonArray.length();
    }
}
