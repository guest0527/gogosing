package com.sangwoo.gogosingapp.Util.ServerUtil;

import com.sangwoo.gogosingapp.Util.Encrypt.AES256Cipher;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by SangWoo on 2016. 8. 19..
 */
public class JoinInfo extends Info{
    private final static String PARAM_EMAIL = "email";
    private final static String PARAM_PASSWD = "pw";
    private final static String PARAM_KAKAO_ID = "kakao_id";
    private final static String PARAM_FACEBOOK_ID = "facebook_id";
    private final static String PARAM_NAME = "name";
    private final static String PARAM_TEL = "tel";
    private final static String PARAM_JIBUN_ADDRESS = "jibunAddress";
    private final static String PARAM_ROAD_ADDRESS = "roadAddress";
    private final static String PARAM_DETAIL_ADDRESS = "detailAddress";
    private final static String PARAM_BIRTH = "brith";



    public void setEmail(String email) {
        map.put(PARAM_EMAIL, email);
    }

    public void setPassword(String password) throws NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        map.put(PARAM_PASSWD, AES256Cipher.AES_Encode(password));
    }

    public void setFacebookID(String facebookID) {
        map.put(PARAM_FACEBOOK_ID, facebookID);
    }

    public void setKakaoID(String kakaoID) {
        map.put(PARAM_KAKAO_ID, kakaoID);
    }

    public void setName(String name) {
        map.put(PARAM_NAME, name);
    }

    public void setPhone(String tel) {
        map.put(PARAM_TEL, tel);
    }

    public void setJibunAddress(String jibunAddress) {
        map.put(PARAM_JIBUN_ADDRESS, jibunAddress);
    }

    public void setRoadAddress(String roadAddress) {
        map.put(PARAM_ROAD_ADDRESS, roadAddress);
    }

    public void setDetailAddress(String detailAddress) {
        map.put(PARAM_DETAIL_ADDRESS, detailAddress);
    }

    public void setBirth(String birth) {
        if(birth == null || birth.isEmpty() || birth.length() == 0){
            return;
        }
        map.put(PARAM_BIRTH, birth);
    }
}
