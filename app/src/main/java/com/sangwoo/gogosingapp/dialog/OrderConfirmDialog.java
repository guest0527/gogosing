package com.sangwoo.gogosingapp.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.MakeOrderInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class OrderConfirmDialog extends SoftkeyHideDialog implements View.OnClickListener {

    private MakeOrderInfo info = null;
    private TextView reservationTime = null;
    private TextView address = null;
    private TextView phone = null;
    private Handler handler = null;
    private onConfirmListener listener = null;


    private OrderConfirmDialog(Context context) {
        super(context);

        setContentView(R.layout.dialog_order_confirm);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN, LayoutParams.FLAG_FULLSCREEN);

        LayoutParams params = this.getWindow().getAttributes();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        this.getWindow().setAttributes((LayoutParams) params);

        init();
    }



    public OrderConfirmDialog(Context context, MakeOrderInfo info) {
        this(context);
        setOrderInfo(info);
    }

    private void init() {
        findViewById(R.id.button_confirm).setOnClickListener(this);
        findViewById(R.id.button_cancle).setOnClickListener(this);

        reservationTime = (TextView) findViewById(R.id.textview_reservation_time);
        address = (TextView) findViewById(R.id.textview_reservation_address);
        phone = (TextView) findViewById(R.id.textview_reservation_phone);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            dismiss();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.button_confirm:
                if(listener != null)
                    listener.onPositive();
                break;
            case R.id.button_cancle:
                if(listener != null)
                    listener.onNegative();
                break;
            default:
                break;
        }
        dismiss();
    }

    public void setOrderInfo(MakeOrderInfo info) {
        this.info = info;

        String time = info.getReservationTime();
        String address = info.getRoadAddress() + " " + info.getDetailAddress();
        String tel = info.getPhone();

        if (time == null)
            time = getContext().getString(R.string.string_order_page_reservation_time_right_away);

        this.reservationTime.setText(time);
        this.address.setText(address);
        this.phone.setText(tel);
    }

    public void setOnConfirmListener(onConfirmListener listener){
        this.listener = listener;
    }

    public interface onConfirmListener{
        public void onPositive();
        public void onNegative();
    }

}
