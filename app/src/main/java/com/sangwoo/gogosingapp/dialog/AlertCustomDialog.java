package com.sangwoo.gogosingapp.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.sangwoo.gogosingapp.R;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class AlertCustomDialog extends SoftkeyHideDialog implements View.OnClickListener {

    private TextView descript = null;


    public AlertCustomDialog(Context context) {
        super(context);

        setContentView(R.layout.dialog_default_alter);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        this.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        init();
    }

    private void init(){
        descript = (TextView) findViewById(R.id.textview_description);

        findViewById(R.id.button_confirm).setOnClickListener(this);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            dismiss();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if(id == R.id.button_confirm)
            dismiss();
    }

    public void setText(String str){
        descript.setText(str);
    }
}
