package com.sangwoo.gogosingapp.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.sangwoo.gogosingapp.R;

/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class TermDialog extends SoftkeyHideDialog implements View.OnClickListener {

    private TextView descript = null;


    public TermDialog(Context context) {
        super(context);

        setContentView(R.layout.dialog_terms);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN, LayoutParams.FLAG_FULLSCREEN);

        LayoutParams params = this.getWindow().getAttributes();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        this.getWindow().setAttributes((LayoutParams) params);

        init();
    }

    private void init(){
        descript = (TextView) findViewById(R.id.textview_terms);

        findViewById(R.id.button_ok).setOnClickListener(this);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            dismiss();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if(id == R.id.button_ok)
            dismiss();
    }

    public void setText(String str){
        descript.setText(str);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }
}
