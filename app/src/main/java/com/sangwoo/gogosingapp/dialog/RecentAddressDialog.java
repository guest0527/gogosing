package com.sangwoo.gogosingapp.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.adapter.RecentAddressListAdapter;


/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class RecentAddressDialog extends SoftkeyHideDialog implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView listView = null;
    private RecentAddressListAdapter adapter = null;
    private AdapterView.OnItemClickListener itemClickListener = null;


    public RecentAddressDialog(Context context) {
        super(context);

        setContentView(R.layout.dialog_recent_address);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN, LayoutParams.FLAG_FULLSCREEN);

        LayoutParams params = this.getWindow().getAttributes();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        this.getWindow().setAttributes((LayoutParams) params);

        init();
    }

    public void setAdapter(RecentAddressListAdapter adapter){
        listView.setAdapter(adapter);
        adapter.notifyDataSetInvalidated();


    }


    private void init() {
        listView = (ListView) findViewById(R.id.listview_recent_address_result);
        listView.setOnItemClickListener(this);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            dismiss();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            default:
                break;
        }
        dismiss();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener clickListener){
        itemClickListener = clickListener;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        dismiss();
        if(itemClickListener != null)
            itemClickListener.onItemClick(adapterView,view,i,l);
    }
}
