package com.sangwoo.gogosingapp.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import com.sangwoo.gogosingapp.GogosingApp;

/**
 * Created by SangWoo on 2016. 8. 7..
 */
public class SoftkeyHideDialog extends Dialog {

    public SoftkeyHideDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null && getWindow().getDecorView() != null) {
            GogosingApp.hideSystemUI(getWindow().getDecorView());
        }


    }

    @Override
    public void onAttachedToWindow() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        super.onAttachedToWindow();
    }

    @Override
    public void show() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        super.show();
    }
}
