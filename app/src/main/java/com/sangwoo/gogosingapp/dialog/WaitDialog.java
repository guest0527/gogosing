package com.sangwoo.gogosingapp.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.sangwoo.gogosingapp.R;

/**
 * Created by SangWoo on 2016. 9. 22..
 */
public class WaitDialog extends SoftkeyHideDialog{

    ProgressBar progressBar = null;

    public WaitDialog(Context context) {
        super(context);
        setContentView(R.layout.dialog_wait);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setIndeterminate(true);
    }
}
