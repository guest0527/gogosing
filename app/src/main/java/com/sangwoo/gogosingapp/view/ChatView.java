package com.sangwoo.gogosingapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sangwoo.gogosingapp.R;

import java.util.ArrayList;


/**
 * Created by SangWoo on 2016-11-16.
 */

public class ChatView extends LinearLayout implements View.OnClickListener, ChildEventListener {

    private ViewGroup rootView;
    private ListView listView;
    private FirebaseDatabase firebaseRealDB = null;
    private DatabaseReference databaseReference = null;
    private EditText editText = null;
    private ChatViewAdapter adapter = null;


    public ChatView(Context context) {
        super(context);
        init();
    }

    public ChatView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        viewInit();
        dbInit();
        adapterInit();
    }

    private void dbInit() {
        firebaseRealDB = FirebaseDatabase.getInstance();
        databaseReference = firebaseRealDB.getReference().child("gogosingChatService");
        databaseReference.addChildEventListener(this);
    }

    private void viewInit() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = (ViewGroup) inflater.inflate(R.layout.layout_chatview, null);
        listView = (ListView) rootView.findViewById(R.id.list_chatView);
        editText = (EditText) rootView.findViewById(R.id.edittext_chat_msg);

        rootView.findViewById(R.id.button_send).setOnClickListener(this);

        addView(rootView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_send:
                ChatData chatData = new ChatData("phone", editText.getText().toString());
                databaseReference.push().setValue(chatData);
                break;
        }
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        if (adapter != null) {

            Iterable<DataSnapshot> it = dataSnapshot.getChildren();

            ChatData data = new ChatData();

            for (DataSnapshot snapshot : it) {
                if (snapshot.getKey().equals("id")) {
                    data.setID((String) snapshot.getValue());
                } else {
                    data.setMsg((String) snapshot.getValue());
                }
            }

            adapter.add(data);
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    public class ChatData {
        private String id;
        private String msg;

        public ChatData() {

        }

        public ChatData(String id, String msg) {
            this.id = id;
            this.msg = msg;
        }

        public void setID(String id) {
            this.id = id;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getID() {
            return id;
        }

        public String getMsg() {
            return msg;
        }
    }

    public abstract class ChatViewAdapter extends BaseAdapter {
        public abstract void add(Object obj);
    }

    private void adapterInit() {
        adapter = new ChatViewAdapter() {
            ArrayList<ChatData> list = new ArrayList<ChatData>();

            @Override
            public void add(Object obj) {
                list.add((ChatData) obj);
            }

            @Override
            public int getCount() {
                return list.size();
            }

            @Override
            public Object getItem(int position) {
                return list.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ViewHolder holder = null;

                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_chatview, null);
                    TextView id = (TextView) convertView.findViewById(R.id.textview_id);
                    TextView msg = (TextView) convertView.findViewById(R.id.textview_msg);

                    holder = new ViewHolder();

                    holder.id = id;
                    holder.msg = msg;

                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }

                ChatData data = (ChatData) getItem(position);

                holder.id.setText(data.getID());
                holder.msg.setText(data.getMsg());

                return convertView;
            }


            class ViewHolder {
                public TextView id;
                public TextView msg;
            }
        };

        listView.setAdapter(adapter);
    }
}
