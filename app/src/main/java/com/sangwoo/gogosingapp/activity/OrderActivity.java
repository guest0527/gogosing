package com.sangwoo.gogosingapp.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.MakeOrderInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.UserDataInfo;
import com.sangwoo.gogosingapp.Util.Util;
import com.sangwoo.gogosingapp.adapter.RecentAddressListAdapter;
import com.sangwoo.gogosingapp.define.IntentAction;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;
import com.sangwoo.gogosingapp.dialog.AlertCustomDialog;
import com.sangwoo.gogosingapp.dialog.OrderConfirmDialog;
import com.sangwoo.gogosingapp.dialog.RecentAddressDialog;
import com.sangwoo.gogosingapp.dialog.TermDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class OrderActivity extends Activity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, TimePickerDialog.OnTimeSetListener, AdapterView.OnItemClickListener {


    private static final int REQUEST_SEARCH_JUSO = 0x1845;
    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = REQUEST_SEARCH_JUSO + 1;

    private Handler handler = null;

    private EditText orderInfo = null;
    private EditText address1 = null;
    private EditText detailAddress = null;
    private EditText orderComment = null;
    private EditText phone = null;


    private TextView reservationTime = null;

    private RadioGroup reservationGroup = null;
    private RadioGroup paymentGroup = null;
    private RadioButton reservatoinTimeSelect = null;

    private String jibunAddress = null;
    private String roadAddress = null;

    private CheckBox agree1 = null;
    private CheckBox agree2 = null;
    private CheckBox agree3 = null;

    private View view_agree1 = null;
    private View view_agree2 = null;
    private View view_agree3 = null;
    private View view_agree4 = null;

    private Button btnRecentAddress = null;

    private MakeOrderInfo.PAYMENT_METHOD payment = MakeOrderInfo.PAYMENT_METHOD.CASH;
    private Calendar reservationDate = null;

    private final static SimpleDateFormat RESERVATION_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private long lastClickTime;

    private String userID = null;

    private int RESERVATION_MIN_MINUTE = 30;

    private UserDataInfo userDataInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        init();
        initHandler();
        initButtonListener();
        initLogin();
    }

    private void init() {
        orderInfo = (EditText) findViewById(R.id.edittext_order_info);
        address1 = (EditText) findViewById(R.id.edittext_search_address);
        detailAddress = (EditText) findViewById(R.id.edittext_detail_address);
        orderComment = (EditText) findViewById(R.id.edittext_order_comment);
        phone = (EditText) findViewById(R.id.edittext_phone);

        reservationTime = (TextView) findViewById(R.id.textview_reservation_time);

        reservationGroup = (RadioGroup) findViewById(R.id.radio_group_reservation);
        paymentGroup = (RadioGroup) findViewById(R.id.radio_group_payment_mathod);
        reservatoinTimeSelect = (RadioButton) findViewById(R.id.radio_reservation_time_select);

        view_agree1 = findViewById(R.id.textview_non_member_agree);
        view_agree2 = findViewById(R.id.layout_non_member_agree1);
        view_agree3 = findViewById(R.id.layout_non_member_agree2);
        view_agree4 = findViewById(R.id.layout_non_member_agree3);

        agree1 = (CheckBox) findViewById(R.id.checkbox_agree1);
        agree2 = (CheckBox) findViewById(R.id.checkbox_agree2);
        agree3 = (CheckBox) findViewById(R.id.checkbox_agree3);
    }

    private void initButtonListener() {


        reservationGroup.setOnCheckedChangeListener(this);
        paymentGroup.setOnCheckedChangeListener(this);

        btnRecentAddress = (Button) findViewById(R.id.button_recent_address);

        btnRecentAddress.setOnClickListener(this);

        findViewById(android.R.id.content).setOnClickListener(GogosingApp.hideKeyboardListener());
        findViewById(R.id.button_find_address).setOnClickListener(this);
        findViewById(R.id.radio_reservation_time_select).setOnClickListener(this);
        findViewById(R.id.button_order).setOnClickListener(this);
        findViewById(R.id.button_terms1).setOnClickListener(this);
        findViewById(R.id.button_terms2).setOnClickListener(this);
        findViewById(R.id.button_terms3).setOnClickListener(this);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

        switch (checkedId) {
            case R.id.radio_reservation_now:
//                reservationDate = new Date(System.currentTimeMillis());
                reservationDate = null;
                reservationTime.setVisibility(View.INVISIBLE);
                break;
            case R.id.radio_payment_method_card:
                payment = MakeOrderInfo.PAYMENT_METHOD.CARD;
                break;
            case R.id.radio_payment_method_cash:
                payment = MakeOrderInfo.PAYMENT_METHOD.CASH;
                break;
            case R.id.radio_payment_method_bank:
                payment = MakeOrderInfo.PAYMENT_METHOD.BANK;
                break;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        long currentTime = System.currentTimeMillis();

        if (lastClickTime + 500 > currentTime)
            return;

        lastClickTime = currentTime;

        switch (id) {
            case R.id.button_recent_address: {
                RecentAddressDialog dialog = new RecentAddressDialog(this);
                RecentAddressListAdapter adapter = new RecentAddressListAdapter();
                if (userDataInfo != null) {
                    for (int i = 0; i < userDataInfo.getRecentAddressListSize(); i++) {
                        adapter.add(userDataInfo.getRecentAddress(i));
                    }
                    dialog.setAdapter(adapter);
                    dialog.setOnItemClickListener(OrderActivity.this);
                    dialog.show();
                }
            }
            break;
            case R.id.button_find_address:
                Intent intent = new Intent(this, SearchAddressActivity.class);
                startActivityForResult(intent, REQUEST_SEARCH_JUSO);
                break;
            case R.id.radio_reservation_time_select:
                showTimePickerDialog();
                break;
            case R.id.button_order:
                if (isValidateOrder()) {

                    OrderConfirmDialog dialog = new OrderConfirmDialog(this, getOrderInfo());
                    dialog.setOnConfirmListener(new OrderConfirmDialog.onConfirmListener() {
                        @Override
                        public void onPositive() {
                            GogosingApp.getApp().getServerUtil().sendOrder(getOrderInfo(), handler);
                        }

                        @Override
                        public void onNegative() {
                            Util.showLongToast(getString(R.string.string_order_cancle_toast));
                        }
                    });

                    dialog.show();
                    //popup

//                    sendOrder();
                }
                break;
            case R.id.button_terms1: {
                TermDialog dialog = new TermDialog(OrderActivity.this);
                dialog.setText(Util.getReadFile(this, "Terms"));
                dialog.show();
            }
            break;
            case R.id.button_terms2: {
                TermDialog dialog = new TermDialog(OrderActivity.this);
                dialog.setText(Util.getReadFile(this, "CollectPrivacyTerms"));
                dialog.show();
            }
            break;
            case R.id.button_terms3: {
                TermDialog dialog = new TermDialog(OrderActivity.this);
                dialog.setText(Util.getReadFile(this, "ProvidingPrivacyTerms"));
                dialog.show();
            }
            break;
            default:
                break;
        }
    }

    private void goneAgree() {
        view_agree1.setVisibility(View.GONE);
        view_agree2.setVisibility(View.GONE);
        view_agree3.setVisibility(View.GONE);
        view_agree4.setVisibility(View.GONE);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

        reservationDate = Calendar.getInstance();
        int currentHourOfDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int currentMin = Calendar.getInstance().get(Calendar.MINUTE);

        if (currentHourOfDay > hourOfDay) {
            reservationDate.add(Calendar.DAY_OF_MONTH, 1);
        }

        if (currentHourOfDay == hourOfDay && (((currentMin + RESERVATION_MIN_MINUTE) < minute) == false)) {
            String str = String.valueOf(RESERVATION_MIN_MINUTE) + getString(R.string.string_order_reservation_time_toast);
            Util.showLongToast(str);
            reservationDate.add(Calendar.MINUTE, 30);

        } else {
            reservationDate.set(Calendar.MINUTE, minute);
        }
        reservationDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
        reservationTime.setText(RESERVATION_DATE_FORMAT.format(reservationDate.getTime()));
        reservationTime.setVisibility(View.VISIBLE);
    }

    private void showTimePickerDialog() {

        int hour = reservationDate == null ? Calendar.getInstance().get(Calendar.HOUR_OF_DAY) : reservationDate.get(Calendar.HOUR_OF_DAY);
        int minute = reservationDate == null ? Calendar.getInstance().get(Calendar.MINUTE) : reservationDate.get(Calendar.MINUTE);


        TimePickerDialog dialog = new TimePickerDialog(this, this, hour, minute, true);
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SEARCH_JUSO:
                if (resultCode == RESULT_OK) {
                    roadAddress = data.getStringExtra(IntentAction.EXTRA_STRING_ROAD_ADDRESS);
                    jibunAddress = data.getStringExtra(IntentAction.EXTRA_STRING_JIBUN_ADDRESS);
                    address1.setText(roadAddress);
                }
                break;
            default:
                break;
        }
    }

    private void initLogin() {
        if (GogosingApp.getApp().isLogin() == false) {
            btnRecentAddress.setVisibility(View.GONE);
            return;
        }
        goneAgree();
        GogosingApp.getApp().getServerUtil().getUserInfo(handler);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
    }

    private void initHandler() {
        handler = new ServerResultHandler(this) {
            @Override
            public void handleMessage(Message msg) {
                int what = msg.what;

                switch (what) {
                    case JsonResultCodeDefine.CODE_GET_USERINFO_OK:

                        userDataInfo = (UserDataInfo) msg.obj;

                        userID = userDataInfo.getID();

                        address1.setText(userDataInfo.getRoadAddress());
                        detailAddress.setText(userDataInfo.getDetailAddress());
                        phone.setText(userDataInfo.getPhone());

                        roadAddress = userDataInfo.getRoadAddress();
                        jibunAddress = userDataInfo.getJibunAddress();
                        break;
                    case JsonResultCodeDefine.CODE_MAKE_ORDER_OK:
                        Util.showLongToast(getString(R.string.string_order_ok_toast));
                        finish();
                        break;
                    case JsonResultCodeDefine.CODE_MAKE_ORDER_FAIL:
                        Util.showLongToast(getString(R.string.string_order_fail_toast));
                        break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }
        };
    }

    private boolean isValidateOrder() {
        //주문내용 주소 연락처, 예약시간 결제수단 입력 확인하기

        if (orderInfo.getText().toString().isEmpty()) {
            showDialog(getString(R.string.string_dialog_alert_order_info));
            return false;
        } else if (jibunAddress == null || roadAddress == null || jibunAddress.isEmpty() || roadAddress.isEmpty() || detailAddress.getText().toString().isEmpty()) {
            showDialog(getString(R.string.string_dialog_alert_juso));
            return false;
        } else if (phone.getText().toString().isEmpty()) {
            showDialog(getString(R.string.string_dialog_alert_phone));
            return false;
        } else if (reservationGroup.getCheckedRadioButtonId() < 0) {
            showDialog(getString(R.string.string_dialog_alert_reservation_time));
            return false;
        } else if (payment == null) {
            showDialog(getString(R.string.string_dialog_alert_payment_method));
            return false;
        } else if (GogosingApp.getApp().isLogin() == false && (agree1.isChecked() == false || agree2.isChecked() == false || agree3.isChecked() == false)) {
            showDialog(getString(R.string.string_order_reservatoin_agree));
            return false;
        }


        return true;
    }

    private void showDialog(String text) {
        AlertCustomDialog dialog = new AlertCustomDialog(this);
        dialog.setText(text);
        dialog.show();
    }

    private MakeOrderInfo getOrderInfo() {

        MakeOrderInfo info = new MakeOrderInfo();

        if (GogosingApp.getApp().isLogin() == true) {
            info.setID(userID);
        }

        info.setOrder(orderInfo.getText().toString());
        info.setJibunAddress(jibunAddress);
        info.setRoadAddress(roadAddress);
        info.setDetailAddress(detailAddress.getText().toString());
        info.setPhone(phone.getText().toString());

        if (reservationDate != null)
            info.setReservationTime(reservationDate.getTime());
        info.setPaymentMethod(payment);
        info.setOrderComment(orderComment.getText().toString());

        return info;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        TextView jibun = (TextView) view.findViewById(R.id.textview_jibun_address);
        TextView road = (TextView) view.findViewById(R.id.textview_road_address);
        TextView detail = (TextView) view.findViewById(R.id.textview_detail_address);

        roadAddress = road.getText().toString();
        jibunAddress = jibun.getText().toString();

        address1.setText(roadAddress);
        detailAddress.setText(detail.getText().toString());
    }
}
