package com.sangwoo.gogosingapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.kakao.auth.AuthType;
import com.kakao.auth.Session;
import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerUtil;
import com.sangwoo.gogosingapp.Util.Util;
import com.sangwoo.gogosingapp.define.IntentAction;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;
import com.sangwoo.gogosingapp.dialog.AlertCustomDialog;

import java.util.Map;


/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    private Handler handler = null;
    private EditText email = null;
    private EditText pw = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        initLoginHandler();
        initKaKaoLogin();
        initFacebookLogin();
        initButtonListener();
    }





    private void init(){
        email = (EditText) findViewById(R.id.edittext_email);
        pw = (EditText) findViewById(R.id.edittext_password);


        TextView tv = (TextView) findViewById(R.id.textview_email_to_join);
        String text = "<u>"+tv.getText()+"</u>";
        tv.setText(Html.fromHtml(text));
    }

    private void testCode(){


        GogosingApp.getApp().getServerUtil().serverSendTest();
//                        MakeOrderInfo info = new MakeOrderInfo();
//
//
//                        info.setReservationTime(new Date());
//                        info.setPhone("01012345678");
//                        info.setPaymentMethod(MakeOrderInfo.PAYMENT_METHOD.CARD);
//                        info.setOrderComment("OrderComent주문코멘트234");
//                        info.setOrder("주문어쩌고");
//                        info.setJibunAddress("JibunAddress");
//                        info.setDetailAddress("DetailAddress");
//                        info.setRoadAddress("RoadAddress");
//                        GogosingApp.getApp().getServerUtil().sendOrder(info);
//                        GogosingApp.getApp().getServerUtil().serverSendTest();
    }

    private void  initLoginHandler(){
        handler = new ServerResultHandler(this){
            @Override
            public void handleMessage(Message msg) {
                int what = msg.what;

                switch (what){
                    case JsonResultCodeDefine.CODE_LGOIN_OK:
                        GogosingApp.getApp().setLogin(true);
                        finish();
                        break;
                    case JsonResultCodeDefine.CODE_KAKAO_SOCIAL_LOGIN_FAIL: {
                        Intent intent = getJoinIntent();
                        Map<String, String> map = (Map<String, String>) msg.obj;
                        intent.putExtra(IntentAction.EXTRA_STRING_KAKAO_ID, map.get(ServerUtil.PARAM_KAKAO_ID));
                        startJoinActivity(intent);
                    }
                        break;
                    case JsonResultCodeDefine.CODE_FACEBOOK_SOCIAL_LOGIN_FAIL: {
                        Intent intent = getJoinIntent();
                        Map<String, String> map = (Map<String, String>) msg.obj;
                        intent.putExtra(IntentAction.EXTRA_STRING_FACEBOOK_ID, map.get(ServerUtil.PARAM_FACEBOOK_ID));
                        startJoinActivity(intent);
                    }
                        break;
                    case JsonResultCodeDefine.CODE_MEMBER_LOGIN_FAIL:
                        showDialog(getString(R.string.string_alert_email_login_fail));
                        break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }
        };

        GogosingApp.getApp().getServerUtil().setLoginHandler(handler);
    }

    private void initButtonListener(){
        findViewById(android.R.id.content).setOnClickListener(GogosingApp.hideKeyboardListener());
        findViewById(R.id.button_kakao_login).setOnClickListener(this);
        findViewById(R.id.button_fb_login).setOnClickListener(this);
        findViewById(R.id.textview_email_to_join).setOnClickListener(this);
        findViewById(R.id.button_email_login).setOnClickListener(this);
    }

    private void initKaKaoLogin(){
        GogosingApp.setCurrentActivity(this);

        GogosingApp.getApp().getKakaoUtil().logoff();
        GogosingApp.getApp().getKakaoUtil().getCurrentSession().addCallback(GogosingApp.getApp().getKakaoUtil().getCallBack());
        GogosingApp.getApp().getKakaoUtil().getCurrentSession().checkAndImplicitOpen();
    }

    private void initFacebookLogin(){
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(GogosingApp.getApp().getFacebookUtil().getCallbackManager(), GogosingApp.getApp().getFacebookUtil().getLoginResultCallback());
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.button_email_login:
                //test code
//                GogosingApp.getApp().getServerUtil().siteLogin("test@gmail.com","qwer");
                if(validateLoginEmail()) {
                    try {
                        GogosingApp.getApp().getServerUtil().siteLogin(email.getText().toString(), pw.getText().toString());
                    }
                    catch (Exception e){
                        showDialog(getString(R.string.string_app_crack));
                    }

                }
                else{

                }
                    //popup 노출
                break;
            case R.id.button_kakao_login:
                Session.getCurrentSession().open(AuthType.KAKAO_TALK, this);
                break;
            case R.id.button_fb_login:

                LoginManager.getInstance().logInWithReadPermissions(this, null);
                break;
            case R.id.textview_email_to_join:
                startJoinActivity(getJoinIntent());
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (GogosingApp.getApp().getKakaoUtil().getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }else if(GogosingApp.getApp().getFacebookUtil().getCallbackManager() != null) {
            GogosingApp.getApp().getFacebookUtil().getCallbackManager().onActivityResult(requestCode, resultCode, data);
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopKakao();
        stopFacebook();
        handler = null;
    }

    private void stopKakao(){
        GogosingApp.getApp().getKakaoUtil().getCurrentSession().removeCallback(GogosingApp.getApp().getKakaoUtil().getCallBack());
    }

    private void stopFacebook(){
        GogosingApp.getApp().getFacebookUtil().stopTokenTracker();
        GogosingApp.getApp().getFacebookUtil().stopProfileTracker();
    }

    private void startJoinActivity(Intent intent){
        startActivity(intent);
    }

    private Intent getJoinIntent(){
        Intent intent = new Intent(this, MemberJoinActivity.class);
        return intent;
    }

    private boolean validateLoginEmail(){

        if(email.getText().toString().isEmpty()){
            showDialog(getString(R.string.string_alert_email_empty));
            return false;
        }


        if(pw.getText().toString().isEmpty()) {
            showDialog(getString(R.string.string_alert_password_empty));
            return false;
        }

        return true;
    }

    private void showDialog(String str){
        AlertCustomDialog dialog = new AlertCustomDialog(this);
        dialog.setText(str);
        dialog.show();
    }

}
