package com.sangwoo.gogosingapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.JusoUtil.JusoData;
import com.sangwoo.gogosingapp.Util.JusoUtil.JusoVO;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.adapter.SearchAddressListAdapter;
import com.sangwoo.gogosingapp.define.IntentAction;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;


/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class SearchAddressActivity extends Activity implements View.OnClickListener, View.OnKeyListener {
    private EditText searchAddress = null;
    private ListView searchResultListView = null;
    private SearchAddressListAdapter adapter = null;
    private Handler searchHandler = null;
    private View searchButton = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_address);

        init();
        initHandler();
        initButtonListener();
    }

    private void init() {
        searchAddress = (EditText) findViewById(R.id.edittext_search_address);
        searchResultListView = (ListView) findViewById(R.id.listview_search_address_result);
        searchButton = findViewById(R.id.button_search);

        searchAddress.setOnKeyListener(this);

        adapter = new SearchAddressListAdapter();

        searchResultListView.setAdapter(adapter);

        searchResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                JusoData data = adapter.getItem(i);
                Intent intent = new Intent();
                intent.putExtra(IntentAction.EXTRA_STRING_ROAD_ADDRESS, data.getRoadAddress());
                intent.putExtra(IntentAction.EXTRA_STRING_JIBUN_ADDRESS, data.getJibunAddres());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        searchHandler = null;
    }

    private void initHandler() {
        searchHandler = new ServerResultHandler(this) {
            @Override
            public void handleMessage(Message msg) {

                switch (msg.what){
                    case JsonResultCodeDefine.CODE_SEARCH_ADDRESS:
                        JusoVO vo = (JusoVO) msg.obj;

                        adapter.listChange(vo.getJusoDataList());
                        adapter.notifyDataSetInvalidated();
                        break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }
        };
    }


    private void initButtonListener() {
        findViewById(R.id.button_search).setOnClickListener(this);
        findViewById(R.id.button_cancle).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.button_search:
                adapter.clear();
                searchAddress(searchAddress.getText().toString());
                break;
            case R.id.button_cancle:
                break;
            default:
                break;
        }

    }

    private void searchAddress(String keyword) {
        GogosingApp.getApp().getServerUtil().searchJuso(keyword, searchHandler);
    }

    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_UP) {
            searchAddress(searchAddress.getText().toString());
            return true;
        }
        return false;
    }
}
