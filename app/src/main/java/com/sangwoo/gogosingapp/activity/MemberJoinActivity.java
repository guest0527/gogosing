package com.sangwoo.gogosingapp.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.JoinInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerUtil;
import com.sangwoo.gogosingapp.Util.Util;
import com.sangwoo.gogosingapp.define.IntentAction;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;
import com.sangwoo.gogosingapp.dialog.AlertCustomDialog;
import com.sangwoo.gogosingapp.dialog.TermDialog;

import java.util.Calendar;
import java.util.Map;


public class MemberJoinActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = "MemberJoinActivity";
    private static final int REQUEST_SEARCH_JUSO = 0x1844;

    //option
    private EditText email = null;
    private EditText password = null;
    private EditText passwordCheck = null;

    //default
    private EditText name = null;
    private EditText phone = null;
    private EditText address1 = null;
    private EditText address2 = null;
    private EditText specialDay = null;

    private CheckBox check_total_agree = null;
    private CheckBox check_agree1 = null;
    private CheckBox check_agree2 = null;
    private CheckBox check_agree3 = null;
    private String kakaoID = null;
    private String facebookID = null;

    private Handler handler = null;

    private String jibunAddress = null;
    private String roadAddress = null;

    private String checkEmail = null;

//    private AlertCustomDialog dialog = null;

    private boolean isEmailCheck = false;
    private boolean isSocial = false;

    private long lastClickTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_join);

        Intent intent = getIntent();


        if (intent != null) {
            facebookID = intent.getStringExtra(IntentAction.EXTRA_STRING_FACEBOOK_ID);
            kakaoID = intent.getStringExtra(IntentAction.EXTRA_STRING_KAKAO_ID);

            if (facebookID != null || kakaoID != null) {
                isSocial = true;
            }
        }


        if (isSocial) {
            isEmailCheck = true;
            showSocialJoin();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        initDefault();
        initButtonListener();
        initHandler();
    }


    private void initHandler() {
        handler = new ServerResultHandler(this) {
            @Override
            public void handleMessage(Message msg) {
                int what = msg.what;

                AlertCustomDialog dialog = new AlertCustomDialog(MemberJoinActivity.this);
                ;


                switch (what) {
                    case JsonResultCodeDefine.CODE_DUPLICATE_ID_CHECK_OK:
                        isEmailCheck = true;
                        dialog.setText(getString(R.string.string_dialog_alter_id_use));
                        checkEmail = (String) ((Map) msg.obj).get(ServerUtil.PARAM_SITE_EMAIL);
                        dialog.show();
                        break;
                    case JsonResultCodeDefine.CODE_DUPLICATE_ID_CHECK_FAIL:
                        isEmailCheck = false;
                        dialog.setText(getString(R.string.string_dialog_alter_id_not_use));
                        dialog.show();
                        break;
                    case JsonResultCodeDefine.CODE_JOIN_OK:
                        dialog.setText(getString(R.string.string_dialog_alter_join_ok));
                        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                finish();
                            }
                        });
                        dialog.show();
                        break;
                    case JsonResultCodeDefine.CODE_JOIN_FAIL:
                        break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
    }

    private void showSocialJoin() {
        findViewById(R.id.layout_input_email).setVisibility(View.GONE);
        findViewById(R.id.layout_email_check).setVisibility(View.GONE);
        findViewById(R.id.layout_input_pw).setVisibility(View.GONE);
        findViewById(R.id.layout_input_pw_check).setVisibility(View.GONE);
        findViewById(R.id.view_line_1).setVisibility(View.GONE);
        findViewById(R.id.layout_social_join_empty).setVisibility(View.INVISIBLE);
    }


    private void initButtonListener() {


        findViewById(android.R.id.content).setOnClickListener(GogosingApp.hideKeyboardListener());
        findViewById(R.id.button_join_ok).setOnClickListener(this);
        findViewById(R.id.button_email_check).setOnClickListener(this);
        findViewById(R.id.button_find_address).setOnClickListener(this);
        findViewById(R.id.button_terms1).setOnClickListener(this);
        findViewById(R.id.button_terms2).setOnClickListener(this);
        findViewById(R.id.button_terms3).setOnClickListener(this);


        check_total_agree = (CheckBox) findViewById(R.id.checkbox_total_agree);
        check_agree1 = (CheckBox) findViewById(R.id.checkbox_agree1);
        check_agree2 = (CheckBox) findViewById(R.id.checkbox_agree2);
        check_agree3 = (CheckBox) findViewById(R.id.checkbox_agree3);

        check_total_agree.setOnCheckedChangeListener(this);
        check_agree1.setOnCheckedChangeListener(this);
        check_agree2.setOnCheckedChangeListener(this);
        check_agree3.setOnCheckedChangeListener(this);

        specialDay.setOnClickListener(this);


    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        int id = compoundButton.getId();

        switch (id) {
            case R.id.checkbox_total_agree:
                checkButtonAll(b);
                break;
            case R.id.checkbox_agree1:
            case R.id.checkbox_agree2:
            case R.id.checkbox_agree3:
                if (b == false) {
                    check_total_agree.setOnCheckedChangeListener(null);
                    check_total_agree.setChecked(b);
                    check_total_agree.setOnCheckedChangeListener(this);
                }
                break;
            default:
                break;
        }

    }

    private void checkButtonAll(boolean b) {
        check_agree1.setChecked(b);
        check_agree2.setChecked(b);
        check_agree3.setChecked(b);
    }

    private void initDefault() {
        name = (EditText) findViewById(R.id.edittext_name);
        phone = (EditText) findViewById(R.id.edittext_phone);
        address1 = (EditText) findViewById(R.id.edittext_address1);
        address2 = (EditText) findViewById(R.id.edittext_address2);

        email = (EditText) findViewById(R.id.edittext_email);
        password = (EditText) findViewById(R.id.edittext_password);
        passwordCheck = (EditText) findViewById(R.id.edittext_password_check);

        specialDay = (EditText) findViewById(R.id.edittext_special_day);
    }

    private String getText(EditText editText) {
        if (editText == null)
            return null;
        return editText.getText().toString();
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();

        long currentTime = System.currentTimeMillis();

        if (lastClickTime + 500 > currentTime)
            return;

        lastClickTime = currentTime;

        switch (id) {
            case R.id.button_join_ok:
                if (isJoinOk())
                    joinOK();
                break;
            case R.id.button_email_check:
                String emailStr = getText(email);
                if (emailStr == null || emailStr.isEmpty()) {
                    AlertCustomDialog dialog = new AlertCustomDialog(this);
                    dialog.setText(getString(R.string.string_dialog_alter_id_null));
                    dialog.show();
                    return;
                }
                GogosingApp.getApp().getServerUtil().duplicateEmailCheck(emailStr, handler);
                break;
            case R.id.button_find_address:
                Intent intent = new Intent(this, SearchAddressActivity.class);
                startActivityForResult(intent, REQUEST_SEARCH_JUSO);
                break;
            case R.id.edittext_special_day:
                showDatePickerDialog();
                break;
            case R.id.button_terms1: {
                TermDialog dialog = new TermDialog(MemberJoinActivity.this);
                dialog.setText(Util.getReadFile(this, "Terms"));
                dialog.show();
            }
                break;
            case R.id.button_terms2: {
                TermDialog dialog = new TermDialog(MemberJoinActivity.this);
                dialog.setText(Util.getReadFile(this, "CollectPrivacyTerms"));
                dialog.show();
            }
                break;
            case R.id.button_terms3: {
                TermDialog dialog = new TermDialog(MemberJoinActivity.this);
                dialog.setText(Util.getReadFile(this, "ProvidingPrivacyTerms"));
                dialog.show();
            }
                break;
            default:
                break;
        }
    }

    private void joinOK() {
        JoinInfo joinInfo = new JoinInfo();

        joinInfo.setEmail(getText(email));
        joinInfo.setName(getText(name));
        joinInfo.setKakaoID(kakaoID);
        joinInfo.setFacebookID(facebookID);

        joinInfo.setPhone(getText(phone));
        joinInfo.setRoadAddress(roadAddress);
        joinInfo.setJibunAddress(jibunAddress);
        joinInfo.setDetailAddress(getText(address2));
        joinInfo.setBirth(getText(specialDay));

        try {
            joinInfo.setPassword(getText(password));
        }catch (Exception e){
            Util.showLongToast(getString(R.string.string_app_crack));
        }
        //todo
//        String birth = getText(birth_year) + "-" + getText(birth_month) + "-" + getText(birth_day);
//        joinInfo.setBirth(birth);

        GogosingApp.getApp().getServerUtil().siteJoin(joinInfo, handler);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_SEARCH_JUSO:
                if (resultCode == RESULT_OK) {
                    roadAddress = data.getStringExtra(IntentAction.EXTRA_STRING_ROAD_ADDRESS);
                    jibunAddress = data.getStringExtra(IntentAction.EXTRA_STRING_JIBUN_ADDRESS);
                    address1.setText(roadAddress);
                }
                break;
            default:
                break;
        }
    }

    private boolean joinDataCheck() {
        if (kakaoID == null && facebookID == null && getText(email) == null)
            return false;
        if (getText(name) == null || getText(phone) == null || jibunAddress == null || roadAddress == null || getText(address2) == null) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alert_item_empty));
            dialog.show();
            return false;
        }
        return true;
    }

    private boolean validateName() {
        String name = getText(this.name);
        if (name == null || name.isEmpty()) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alter_id_not_check));
            dialog.show();
            return false;
        }
        return true;
    }

    private boolean validatePassword() {


        if (isSocial)
            return true;

        String password = getText(this.password);
        String passwordCheck = getText(this.passwordCheck);

        if (password == null || passwordCheck == null || (password.equals(passwordCheck) == false) || (Util.validatePassword(password) == false)) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alert_password));
            dialog.show();
            return false;
        }

        return true;
    }


    private boolean validateEmail() {
        if(isSocial)
            return true;

        String email = getText(this.email);

        if (email == null || email.isEmpty()) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alter_id_null));
            dialog.show();
            return false;
        }

        if (Util.validateEmail(email) == false) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alter_id_not_vaildate));
            dialog.show();
            return false;
        }

        if (isEmailCheck == false || (isSocial == false && email.equals(checkEmail

        ) == false)) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alter_id_not_check));
            return false;
        }
        return true;
    }

    private boolean validateAgree() {
        boolean isCheck = check_agree1.isChecked() && check_agree2.isChecked() && check_agree3.isChecked();
        if (isCheck == false) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alter_not_agree));
            dialog.show();
        }
        return isCheck;
    }

    private boolean isJoinOk() {
        return validateEmail() && validatePassword() && joinDataCheck() && validateAgree() && validateName();
    }

    private void showDatePickerDialog() {
        DatePickerDialog dialog = new DatePickerDialog(this, this, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        month = month + 1;
        specialDay.setText(year + "-" + String.format("%02d", month) + "-" + String.format("%02d", day));
    }
}
