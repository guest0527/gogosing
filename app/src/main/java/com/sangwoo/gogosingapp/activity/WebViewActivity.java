package com.sangwoo.gogosingapp.activity;

import android.app.Activity;
import android.os.Bundle;

import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.view.WebView;

/**
 * Created by SangWoo on 2016. 11. 29..
 */
public class WebViewActivity extends Activity{

    private static final String QnAURL = "http://52.78.69.106/admin/faq/mobileBoard.html";

    private WebView webView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        init();
    }

    private void init(){
        webView = (WebView) findViewById(R.id.webView);
        webView.loadUrl(QnAURL);
    }
}
