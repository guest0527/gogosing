package com.sangwoo.gogosingapp.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.ModifyInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.UserDataInfo;
import com.sangwoo.gogosingapp.Util.Util;
import com.sangwoo.gogosingapp.define.IntentAction;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;
import com.sangwoo.gogosingapp.dialog.AlertCustomDialog;

import java.util.Calendar;


public class MemberModifyActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = "MemberJoinActivity";
    private static final int REQUEST_SEARCH_JUSO = 0x1844;

    //option
    private EditText change_password = null;
    private EditText change_passwordCheck = null;

    //default
    private EditText passwrod = null;
    private EditText phone = null;
    private EditText address1 = null;
    private EditText address2 = null;
    private EditText specialDay = null;

    private TextView memberName = null;

    private Handler handler = null;

    private String jibunAddress = null;
    private String roadAddress = null;

    private long lastClickTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_modify);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        initDefault();
        initButtonListener();
        initHandler();
        updateUserInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
    }

    private void initHandler() {
        handler = new ServerResultHandler(this) {
            @Override
            public void handleMessage(Message msg) {
                int what = msg.what;
                switch (what) {
                    case JsonResultCodeDefine.CODE_MODIFY_OK: {
                        AlertCustomDialog dialog = new AlertCustomDialog(MemberModifyActivity.this);
                        ;
                        dialog.setText(getString(R.string.string_dialog_alter_modify_ok));
                        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                finish();
                            }
                        });
                        dialog.show();
                    }
                    break;
                    case JsonResultCodeDefine.CODE_GET_USERINFO_OK:
                        UserDataInfo userDataInfo = (UserDataInfo) msg.obj;
                        memberName.setText(userDataInfo.getName() + getString(R.string.string_member_modify_name_description));
                        phone.setHint(userDataInfo.getPhone());
                        address1.setHint(userDataInfo.getRoadAddress());
                        address2.setHint(userDataInfo.getDetailAddress());
                        specialDay.setHint(userDataInfo.getSpcialDay());
                        break;
                    case JsonResultCodeDefine.CODE_MODIFY_FAIL: {
                        AlertCustomDialog dialog = new AlertCustomDialog(MemberModifyActivity.this);
                        dialog.setText(getString(R.string.string_dialog_alter_modify_fail));
                        dialog.show();
                    }
                    break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }
        };
    }

    private void updateUserInfo() {
        GogosingApp.getApp().getServerUtil().getUserInfo(handler);
    }


    private void initButtonListener() {
        findViewById(R.id.button_modifiy_ok).setOnClickListener(this);
        findViewById(R.id.button_find_address).setOnClickListener(this);

        specialDay.setOnClickListener(this);
    }

    private void initDefault() {
        phone = (EditText) findViewById(R.id.edittext_phone);
        address1 = (EditText) findViewById(R.id.edittext_address1);
        address2 = (EditText) findViewById(R.id.edittext_address2);

        passwrod = (EditText) findViewById(R.id.edittext_password);
        change_password = (EditText) findViewById(R.id.edittext_change_password);
        change_passwordCheck = (EditText) findViewById(R.id.edittext_password_check);

        specialDay = (EditText) findViewById(R.id.edittext_special_day);

        memberName = (TextView) findViewById(R.id.textview_member_name);
    }

    private String getText(EditText editText) {
        if (editText == null)
            return null;
        return editText.getText().toString();
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();

        long currentTime = System.currentTimeMillis();

        if (lastClickTime + 500 > currentTime)
            return;

        lastClickTime = currentTime;

        switch (id) {
            case R.id.button_find_address:
                Intent intent = new Intent(this, SearchAddressActivity.class);
                startActivityForResult(intent, REQUEST_SEARCH_JUSO);
                break;
            case R.id.edittext_special_day:
                showDatePickerDialog();
                break;
            case R.id.button_modifiy_ok:
                if (validateChangeMemberDate())
                    GogosingApp.getApp().getServerUtil().updateUserInfo(createModifyInfo(), handler);
                //sendMemberData;
                break;
            default:
                break;
        }
    }

    private ModifyInfo createModifyInfo() {
        ModifyInfo info = new ModifyInfo();


        info.setJibunAddress(jibunAddress);
        info.setRoadAddress(roadAddress);
        info.setDetailAddress(getText(address2));
        info.setBirth(getText(specialDay));
        info.setPhone(getText(phone));

        try {
            info.setPassword(getText(passwrod));
            if (validateChangePassword()) {
                info.setChangePassword(getText(change_password));
            }
        }catch (Exception e){
            Util.showLongToast(getString(R.string.string_app_crack));
        }
        return info;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_SEARCH_JUSO:
                if (resultCode == RESULT_OK) {
                    roadAddress = data.getStringExtra(IntentAction.EXTRA_STRING_ROAD_ADDRESS);
                    jibunAddress = data.getStringExtra(IntentAction.EXTRA_STRING_JIBUN_ADDRESS);
                    address1.setText(roadAddress);
                }
                break;
            default:
                break;
        }
    }

    private boolean validateChangeMemberDate() {
        return getText(passwrod).isEmpty() == false ? true : false;
    }

    private boolean validateChangePassword() {

        if (change_password.getVisibility() == View.GONE)
            return true;

        String password = getText(this.change_password);
        String passwordCheck = getText(this.change_passwordCheck);

        if (password == null || passwordCheck == null || (password.equals(passwordCheck) == false) || (Util.validatePassword(password) == false)) {
            AlertCustomDialog dialog = new AlertCustomDialog(this);
            dialog.setText(getString(R.string.string_dialog_alert_password));
            dialog.show();
            return false;
        }
        return true;
    }

    private void showDatePickerDialog() {
        DatePickerDialog dialog = new DatePickerDialog(this, this, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        month = month + 1;
        specialDay.setText(year + "-" + String.format("%02d", month) + "-" + String.format("%02d", day));
    }
}
