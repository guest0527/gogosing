package com.sangwoo.gogosingapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.FBUtil.FBCallBack;
import com.sangwoo.gogosingapp.activity.MemberJoinActivity;
import com.sangwoo.gogosingapp.define.IntentAction;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;

import org.w3c.dom.Text;


/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class DetailActivity extends Activity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_item);


        init(getIntent());
        initButtonListener();
    }

    private void initButtonListener(){
        findViewById(R.id.layout_order_button).setOnClickListener(this);
    }




    private void init(Intent intent){
        if(intent == null)
            return;

        TextView title = (TextView) findViewById(R.id.textview_menu_title);
        ImageView descriptionImage = (ImageView) findViewById(R.id.imageview_descript_image);
        TextView descriptionText = (TextView) findViewById(R.id.textview_description);

        title.setText(intent.getStringExtra(IntentAction.EXTRA_STRING_DETAIL_TITLE));
        descriptionText.setText(intent.getStringExtra(IntentAction.EXTRA_STRING_DETAIL_DESCRIPTION)+getString(R.string.string_detail_order_descript_coment));
        descriptionImage.setBackgroundResource(intent.getIntExtra(IntentAction.EXTRA_INT_DETAIL_DESCRIPTION_IMAGE_RES_ID,0));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.layout_order_button:
                startActivity(new Intent(this, OrderActivity.class));
                break;
            default:
                break;
        }
    }
}
