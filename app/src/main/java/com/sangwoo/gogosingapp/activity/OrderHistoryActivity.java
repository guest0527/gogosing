package com.sangwoo.gogosingapp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.GetOrderDataInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.adapter.OrderHistoryListAdapter;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;


/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class OrderHistoryActivity extends Activity {

    private ListView listView = null;
    private Handler handelr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        init();
        initHandler();

    }

    @Override
    protected void onResume() {
        super.onResume();

        GogosingApp.getApp().getServerUtil().getRecentOrder(handelr);
    }

    private void init() {
        listView = (ListView) findViewById(R.id.listview_order_history);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handelr = null;
    }

    private void initHandler() {
        handelr = new ServerResultHandler(this) {
            @Override
            public void handleMessage(Message msg) {

                switch (msg.what) {
                    case JsonResultCodeDefine.CODE_GET_ORDER_LIST_OK:

                        GetOrderDataInfo info = (GetOrderDataInfo) msg.obj;

                        OrderHistoryListAdapter adapter = new OrderHistoryListAdapter();

                        for (int i = 0; i < info.getOrderHistorySize(); i++) {
                            adapter.add(info.getOrderDetailInfo(i));
                        }

                        listView.setAdapter(adapter);
                        break;
                    case JsonResultCodeDefine.CODE_GET_ORDER_LIST_FAIL:
                        break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }
        };
    }
}
