package com.sangwoo.gogosingapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sangwoo.gogosingapp.GogosingApp;
import com.sangwoo.gogosingapp.R;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.NoticeInfo;
import com.sangwoo.gogosingapp.Util.ServerUtil.ServerResultHandler;
import com.sangwoo.gogosingapp.Util.ServerUtil.ResultCallBack.ResultDataInfo.UserDataInfo;
import com.sangwoo.gogosingapp.Util.Util;
import com.sangwoo.gogosingapp.define.IntentAction;
import com.sangwoo.gogosingapp.define.JsonResultCodeDefine;


/**
 * Created by SangWoo on 2016. 7. 31..
 */
public class MainActivity extends Activity implements View.OnClickListener, DrawerLayout.DrawerListener, View.OnTouchListener, GogosingApp.OnLoginStatusListener {

    private final static String YELLOW_ID_URL = "http://plus.kakao.com/home/@고고씽";


    private DrawerLayout drawerLayout = null;
    private View leftView, rightView = null;
    private TextView name = null;
    private TextView email = null;
    private ImageButton userInfoEditView = null;
    private ImageButton loginStatusView = null;
    private LinearLayout noticeLayout = null;


    private Handler handler = null;


    private long lastClickTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playSplash();
        init();
        initButtonListener();
        initHandler();
        updateNotice();
        removoSplash();
    }

    private void removoSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                View splashLayout = findViewById(R.id.layout_splash);
                ViewGroup rootView = (ViewGroup) findViewById(R.id.layout_root);
                rootView.removeView(splashLayout);
                rootView.invalidate();
            }
        }, 2000);
    }

    private void playSplash() {
        AnimationDrawable ani = (AnimationDrawable) ((ImageView) findViewById(R.id.imageview_animation)).getBackground();
        ani.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUserInfo();
        updateLoginStatus();
    }

    private void updateLoginStatus() {
        OnLoginStatus(GogosingApp.getApp().isLogin());
    }

    @Override
    protected void onPause() {
        super.onPause();
        drawerLayout.closeDrawers();
    }

    private void init() {

        noticeLayout = (LinearLayout) findViewById(R.id.layout_notice);
        drawerLayout = (DrawerLayout) findViewById(R.id.layout_drawer);
        leftView = findViewById(R.id.layout_left_menu);
        rightView = findViewById(R.id.layout_right_menu);

        name = (TextView) findViewById(R.id.text_name);
        email = (TextView) findViewById(R.id.text_mail);

        loginStatusView = (ImageButton) findViewById(R.id.button_logout);
        userInfoEditView = (ImageButton) findViewById(R.id.button_edit);

        drawerLayout.addDrawerListener(this);

        GogosingApp.getApp().setOnLoginStatusListener(this);
    }

    private void initButtonListener() {
        findViewById(R.id.button_topMenu).setOnClickListener(this);
        findViewById(R.id.button_topCall).setOnClickListener(this);
        findViewById(R.id.layout_call).setOnClickListener(this);
        findViewById(R.id.button_menu_close).setOnClickListener(this);
        findViewById(R.id.button_menu_sub_1).setOnClickListener(this);
        findViewById(R.id.button_menu_sub_2).setOnClickListener(this);
        findViewById(R.id.button_menu_sub_3).setOnClickListener(this);
        findViewById(R.id.button_menu_sub_4).setOnClickListener(this);
        findViewById(R.id.button_menu_sub_5).setOnClickListener(this);
        findViewById(R.id.button_menu_sub_6).setOnClickListener(this);
        findViewById(R.id.button_order).setOnClickListener(this);
        findViewById(R.id.button_add_kakao).setOnClickListener(this);
        findViewById(R.id.button_order_history).setOnClickListener(this);
        findViewById(R.id.text_name).setOnClickListener(this);
        findViewById(R.id.layout_kakao).setOnClickListener(this);

        findViewById(R.id.button_alarm).setOnClickListener(this);
        findViewById(R.id.button_setting).setOnClickListener(this);
        findViewById(R.id.button_coupon).setOnClickListener(this);
        findViewById(R.id.button_faq).setOnClickListener(this);
        findViewById(R.id.view_transfer).setOnClickListener(this);

        findViewById(R.id.button_rightview).setOnClickListener(this);

        loginStatusView.setOnClickListener(this);
        userInfoEditView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        long currentTime = System.currentTimeMillis();

        if (lastClickTime + 500 > currentTime)
            return;

        lastClickTime = currentTime;

        switch (id) {
            case R.id.button_topCall:
            case R.id.layout_call: {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.string_call_number)));
                startActivity(intent);
            }
            break;
            case R.id.button_topMenu:
                drawerLayout.openDrawer(leftView);
                break;
            case R.id.button_menu_close:
                drawerLayout.closeDrawer(leftView);
                break;
            case R.id.button_menu_sub_1:
                startDetailActivity(getString(R.string.string_menu_category_sub1), getString(R.string.string_menu_category_sub1_description), R.drawable.detail_order_03);
                break;
            case R.id.button_menu_sub_2:
                startDetailActivity(getString(R.string.string_menu_category_sub2), getString(R.string.string_menu_category_sub2_description), R.drawable.detail_eatout_03);
                break;
            case R.id.button_menu_sub_3:
                startDetailActivity(getString(R.string.string_menu_category_sub3), getString(R.string.string_menu_category_sub3_description), R.drawable.detail_big_03);
                break;
            case R.id.button_menu_sub_4:
                startDetailActivity(getString(R.string.string_menu_category_sub4), getString(R.string.string_menu_category_sub4_description), R.drawable.detail_pet_03);
                break;
            case R.id.button_menu_sub_5:
                startDetailActivity(getString(R.string.string_menu_category_sub5), getString(R.string.string_menu_category_sub5_description), R.drawable.detail_life_03);
                break;
            case R.id.button_menu_sub_6:
                startDetailActivity(getString(R.string.string_menu_category_sub6), getString(R.string.string_menu_category_sub6_description), R.drawable.detail_custom_03);
                break;
            case R.id.button_order: {
                Intent intent = new Intent(this, OrderActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.button_order_history: {
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.layout_kakao:
            case R.id.button_add_kakao: {
                Uri uri = Uri.parse(YELLOW_ID_URL);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }

            break;
            case R.id.text_name:
                if (GogosingApp.getApp().isLogin() == false) {
                    startLoginActivity();
                }
                break;
            case R.id.button_alarm:
            case R.id.button_setting:
            case R.id.button_coupon:
                Util.showShortToast(getString(R.string.string_not_supported_message));
                break;
            case R.id.button_faq: {
                Intent intent = new Intent(this, WebViewActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.view_transfer:
                drawerLayout.closeDrawers();
                break;
            case R.id.button_logout:
                if (GogosingApp.getApp().isLogin()) {
                    GogosingApp.getApp().getServerUtil().siteLogout();
                } else {
                    startLoginActivity();
                }

                break;
            case R.id.button_edit: {
                Intent intent = new Intent(this, MemberModifyActivity.class);
                startActivity(intent);
            }
            case R.id.button_rightview:
                if(!drawerLayout.isDrawerOpen(rightView))
                    drawerLayout.openDrawer(rightView);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
    }


    private void updateNotice() {
        GogosingApp.getApp().getServerUtil().getNotice(handler);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        drawerView.setOnTouchListener(this);
        int id = drawerView.getId();

        switch (id) {
            case R.id.layout_right_menu:
                updateNotice();
                break;
            case R.id.layout_left_menu:
                updateUserInfo();
                break;
        }


//        changeTextView(GogosingApp.getApp().isLogin());
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        drawerView.setOnTouchListener(null);
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id = view.getId();
        if (id == R.id.layout_left_menu || id == R.id.layout_right_menu)
            return true;
        return false;
    }


    private void startDetailActivity(String title, String descriptin, int imageResId) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(IntentAction.EXTRA_STRING_DETAIL_TITLE, title);
        intent.putExtra(IntentAction.EXTRA_STRING_DETAIL_DESCRIPTION, descriptin);
        intent.putExtra(IntentAction.EXTRA_INT_DETAIL_DESCRIPTION_IMAGE_RES_ID, imageResId);
        startActivity(intent);
    }

    private void updateUserInfo() {
        GogosingApp.getApp().getServerUtil().getUserInfo(handler);
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    private void initHandler() {
        handler = new ServerResultHandler(this, false) {
            @Override
            public void handleMessage(Message msg) {
                int what = msg.what;
                switch (what) {
                    case JsonResultCodeDefine.CODE_GET_USERINFO_OK:
                        UserDataInfo userDataInfo = (UserDataInfo) msg.obj;
                        name.setText(userDataInfo.getName() + getString(R.string.string_left_menu_name_unit));
                        String email = userDataInfo.getEmail();
                        if (email != null && email.equals("null") == false) {
                            MainActivity.this.email.setText(email);
                        } else {
                            MainActivity.this.email.setText(R.string.string_left_social_login);
                        }
                        break;
                    case JsonResultCodeDefine.CODE_GET_USERINFO_FAIL: {
                        name.setText(R.string.string_left_menu_login_use);
                    }
                    case JsonResultCodeDefine.CODE_GET_NOTICE_OK:
                        NoticeInfo noticeInfo = (NoticeInfo)msg.obj;
                        noticeViewUpdate(noticeInfo);
                        break;
                    case JsonResultCodeDefine.CODE_GET_NOTICE_FAIL:
                        break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }
        };
    }

    private void noticeViewUpdate(NoticeInfo info){
        if(info != null &&info.getNoticeLength() != 0){
            noticeLayout.removeAllViews();

            for(int i=0;i<info.getNoticeLength();i++){
                String notice = info.getNotice(i);

                TextView tv = new TextView(this);
                tv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tv.setGravity(Gravity.LEFT);
                tv.setText(notice);
                noticeLayout.addView(tv);
            }
            noticeLayout.requestLayout();
        }
    }

    @Override
    public void OnLoginStatus(boolean isLogin) {
        if (isLogin) {
            loginStatusView.setImageResource(R.drawable.button_left_menu_logout);
            userInfoEditView.setVisibility(View.VISIBLE);
        } else {

            loginStatusView.setImageResource(R.drawable.button_left_menu_login);
            userInfoEditView.setVisibility(View.GONE);
            name.setText(R.string.string_left_menu_login_use);
            email.setText("");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
    }
}


